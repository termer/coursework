SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `StudyLoad` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `StudyLoad` ;

-- -----------------------------------------------------
-- Table `StudyLoad`.`Lecturer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`Lecturer` (
  `idLecturer` INT NOT NULL AUTO_INCREMENT ,
  `FIO` VARCHAR(128) NULL ,
  `rate` INT NULL COMMENT 'ставка' ,
  PRIMARY KEY (`idLecturer`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`Subject`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`Subject` (
  `idSubject` INT NOT NULL AUTO_INCREMENT ,
  `subjectName` VARCHAR(45) NULL ,
  `code` VARCHAR(45) NULL ,
  PRIMARY KEY (`idSubject`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`Semester`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`Semester` (
  `idSemester` INT NOT NULL AUTO_INCREMENT ,
  `labTime` INT NULL ,
  `lecTime` INT NULL COMMENT 'lectures' ,
  `courseWorkTime` INT NULL ,
  `consultationTime` INT NULL ,
  `examinationTime` INT NULL ,
  `creditTime` INT NULL COMMENT 'Зачет' ,
  `semester` INT NULL ,
  `practiceTime` INT NULL ,
  `idSubject` INT NOT NULL ,
  `weekTime` INT NULL ,
  PRIMARY KEY (`idSemester`) ,
  INDEX `fk_Semester_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_Semester_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `StudyLoad`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`StudentsGroup`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`StudentsGroup` (
  `idGroup` INT NOT NULL AUTO_INCREMENT ,
  `groupName` VARCHAR(8) NULL ,
  `admissionYear` YEAR NULL ,
  `contractCount` INT NULL ,
  `budgetCount` INT NULL ,
  PRIMARY KEY (`idGroup`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`StudyLoad`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`StudyLoad` (
  `idStudyLoad` INT NOT NULL AUTO_INCREMENT ,
  `idLecturer` INT NOT NULL ,
  `idGroup` INT NOT NULL ,
  `idSubject` INT NOT NULL ,
  PRIMARY KEY (`idStudyLoad`) ,
  INDEX `fk_StudyLoad_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_StudyLoad_Group1_idx` (`idGroup` ASC) ,
  INDEX `fk_StudyLoad_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_StudyLoad_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `StudyLoad`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StudyLoad_Group1`
    FOREIGN KEY (`idGroup` )
    REFERENCES `StudyLoad`.`StudentsGroup` (`idGroup` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_StudyLoad_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `StudyLoad`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`Pair`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`Pair` (
  `idPair` INT NOT NULL AUTO_INCREMENT ,
  `idStudyLoad` INT NOT NULL ,
  `day` INT NULL ,
  `number` INT NULL COMMENT 'какая пара по очередности(1, 2, 3, 4)' ,
  PRIMARY KEY (`idPair`) ,
  INDEX `fk_Schedule_StudyLoad1_idx` (`idStudyLoad` ASC) ,
  CONSTRAINT `fk_Schedule_StudyLoad1`
    FOREIGN KEY (`idStudyLoad` )
    REFERENCES `StudyLoad`.`StudyLoad` (`idStudyLoad` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `StudyLoad`.`WorkedOutPair`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `StudyLoad`.`WorkedOutPair` (
  `idWorkedOutPair` INT NOT NULL AUTO_INCREMENT ,
  `status` INT NULL ,
  `idPair` INT NOT NULL ,
  `date` DATE NULL ,
  PRIMARY KEY (`idWorkedOutPair`) ,
  INDEX `fk_WorkedOutPair_Pair1_idx` (`idPair` ASC) ,
  CONSTRAINT `fk_WorkedOutPair_Pair1`
    FOREIGN KEY (`idPair` )
    REFERENCES `StudyLoad`.`Pair` (`idPair` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `StudyLoad` ;

-- -----------------------------------------------------
-- procedure routine1
-- -----------------------------------------------------

DELIMITER $$
USE `StudyLoad`$$
CREATE PROCEDURE `StudyLoad`.`routine1` ()
BEGIN
	SELECT l.idLecturer, l.FIO, sg.groupName,  s.subjectName, sm.labTime, sm.lecTime, sm.courseWorkTime, sm.consultationTime,
        sm.examinationTime, sm.creditTime, sm.practiceTime,
        sm2.labTime, sm2.lecTime, sm2.courseWorkTime, sm2.consultationTime,
        sm2.examinationTime, sm2.creditTime, sm2.practiceTime  
    FROM Lecturer l, StudyLoad sl, StudentsGroup sg, Subject s, Semester sm, Semester sm2 
 WHERE l.idLecturer = sl.idLecturer and
    s.idSubject = sl.idSubject and
    sg.idGroup = sl.idGroup and
    sm.idSubject = s.idSubject and
    sm2.idSubject = s.idSubject and
    sm.idSemester <> sm2.idSemester and
    sm.semester = 1 and
    sm2.semester = 2;
END$$

DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
