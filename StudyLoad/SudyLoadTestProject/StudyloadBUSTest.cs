﻿using StudyLoad.BUS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using StudyLoad.VO;

namespace SudyLoadTestProject
{
    
    
    /// <summary>
    ///This is a test class for StudyloadBUSTest and is intended
    ///to contain all StudyloadBUSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class StudyloadBUSTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///A test for GetBindedSubject
        ///</summary>
        [TestMethod()]
        [DeploymentItem("StudyLoad.exe")]
        public void GetBindedSubjectTest()
        {
            StudyloadBUS_Accessor target = new StudyloadBUS_Accessor();

            DataTable table= new DataTable("fake table");
            table.Columns.AddRange(new[]
                                       {
                                           new DataColumn("idSubject", typeof(int)),
                                           new DataColumn("idGroup", typeof(int)),
                                           new DataColumn("idLecturer", typeof(int)),
                                           new DataColumn("idSubject", typeof(int)),
                                       });
            DataRow row = table.NewRow();
        //    row.


            BindedSubjectVO expected = null;
            BindedSubjectVO actual;
            actual = target.GetBindedSubject(row);
         
            
            
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetBindedSubjects
        ///</summary>
        [TestMethod()]
        public void GetBindedSubjectsTest()
        {
            StudyloadBUS target = new StudyloadBUS(); // TODO: Initialize to an appropriate value
            LecturerVO lecturer = null; // TODO: Initialize to an appropriate value
            BindedSubjectVO[] expected = null; // TODO: Initialize to an appropriate value
            BindedSubjectVO[] actual;
            actual = target.GetBindedSubjectsForLecturer(lecturer);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for UpdateInDataBase
        ///</summary>
        [TestMethod()]
        public void UpdateInDataBaseTest()
        {
            StudyloadBUS target = new StudyloadBUS(); // TODO: Initialize to an appropriate value
            BindedSubjectVO data = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.UpdateInDataBase(data);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
