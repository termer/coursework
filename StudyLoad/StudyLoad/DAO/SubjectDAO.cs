﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class SubjectDAO : DAObject
    {
        public SubjectDAO()
        {
        }

        public DataTable GetSubjects(int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery("SELECT * FROM Subject s WHERE s.Year = @y", new MySqlParameter[] {new MySqlParameter("y", year) });
            End();

            return p;
        }

        public DataTable GetSemester(int idSubject, int semester)
        {
            Begin();
            var p =
                MySqlHelper.ExecuteSelectQuery(
                    "SELECT * FROM Semester sem WHERE sem.idSubject = @id and sem.Semester = @semester",
                    new[]
                        {
                            new MySqlParameter("id", idSubject),
                            new MySqlParameter("semester", semester)
                        });
            End();
            return p;
        }

        public DataTable GetDependableLecturers(int id)
        {
            Begin();

            var table = MySqlHelper.ExecuteSelectQuery(
                @"SELECT * FROM Lecturer l, StudyLoad sl, Subject s 
                           WHERE s.idSubject = @id and
                                 sl.idSubject = s.idSubject and
                                 l.idLecturer = sl.idLecturer
                           GROUP BY l.idLecturer",
                new[]
                    {
                        new MySqlParameter("id", id)
                    });
            End();
            return table;
        }

        public int InsertSubjectToDb(string name, string code, int subjectTypeId,
                                     int weekTime1, int lecTime1, int labTime1, int courseWorkTime1, int consultationTime1,
                                     int examinationTime1, int creditTime1, int practiceTime1, int overallTime1,
                                     int weekTime2, int lecTime2, int labTime2, int courseWorkTime2, int consultationTime2,
                                     int examinationTime2, int creditTime2, int practiceTime2, int overallTime2, int year)
        {
            Begin();
            MySqlHelper.Insert("Subject", new[]
                                              {
                                                  "idSubjectType",
                                                  "subjectName",
                                                  "code",
                                                  "Year"
                                              },
                               new object[]
                                   {
                                       subjectTypeId,
                                       name,
                                       code,
                                       year
                                   });
            int subjectId = MySqlHelper.GetLastInsertId();

            InsertSemester(1, weekTime1, lecTime1, labTime1, courseWorkTime1, consultationTime1, examinationTime1, creditTime1,
                           practiceTime1,overallTime1, subjectId);
            InsertSemester(2, weekTime2, lecTime2, labTime2, courseWorkTime2, consultationTime2, examinationTime2, creditTime2,
                           practiceTime2, overallTime2, subjectId);
            
            End();
            return subjectId;
        }


        public void UpdateSubjectInDb(int id, string name, string code, int subjectTypeId,
                                        int sid, int weekTime, int lecTime, int labTime, int courseWorkTime, int consultationTime, int examinationTime, int creditTime, int practiceTime, int overallTime,
                                        int sid1, int weekTime1, int lecTime1, int labTime1, int courseWorkTime1, int consultationTime1, int examinationTime1, int creditTime1, int practiceTime1, int overallTime1, int year)
        {
            Begin();
            MySqlHelper.Update("Subject", new[]
                                              {
                                                  "idSubjectType",
                                                  "subjectName",
                                                  "code",
                                                  "Year"
                                              },
                                          new object[]
                                              {
                                                  subjectTypeId,
                                                  name,
                                                  code,
                                                  year
                                              }, "idSubject = " + id);

            UpdateSemester(sid, weekTime, lecTime, labTime, courseWorkTime, consultationTime, examinationTime, creditTime, practiceTime, overallTime);
            UpdateSemester(sid1, weekTime1, lecTime1, labTime1, courseWorkTime1, consultationTime1, examinationTime1, creditTime1, practiceTime1, overallTime1);

            End();
        }

        private void InsertSemester(int semester, int weekTime1, int lecTime1, int labTime1, int courseWorkTime1, int consultationTime1,
                                    int examinationTime1,
                                    int creditTime1, int practiceTime1, int overallTime, int idSubject)
        {
            MySqlHelper.Insert("Semester", new[]
                                               {
                                                   "semester",
                                                   "idSubject",
                                                   "weekTime",
                                                   "labTime",
                                                   "lecTime",
                                                   "courseWorkTime",
                                                   "practiceTime",
                                                   "creditTime",
                                                   "consultationTime",
                                                   "examinationTime",
                                                   "overallTime"
                                               },
                               new object[]
                                   {
                                       semester,
                                       idSubject,
                                       weekTime1,
                                       lecTime1,
                                       labTime1,
                                       courseWorkTime1,
                                       consultationTime1,
                                       examinationTime1,
                                       creditTime1,
                                       practiceTime1,
                                       overallTime
                                   });
        }

        public DataTable GetSubjectWithId(int id)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery("SELECT * FROM Subject WHERE idSubject = @id",
                                                   new[]
                                                       {
                                                           new MySqlParameter("id", id)
                                                       });
            End();
            return p;
        }


        private void UpdateSemester(int id, int weekTime, int lecTime1, int labTime1, int courseWorkTime1, int consultationTime1,
                                    int examinationTime1,
                                    int creditTime1, int practiceTime1, int overallTime)
        {

            MySqlHelper.Update("Semester", new[]
                                               {
                                                   "weekTime",
                                                   "lecTime",
                                                   "labTime",
                                                   "courseWorkTime",
                                                   "consultationTime",
                                                   "examinationTime",
                                                   "creditTime",
                                                   "practiceTime",
                                                   "overallTime"
                                               },
                               new object[]
                                   {
                                       weekTime,
                                       lecTime1,
                                       labTime1,
                                       courseWorkTime1,
                                       consultationTime1,
                                       examinationTime1,
                                       creditTime1,
                                       practiceTime1,
                                       overallTime
                                   },"idSemester = " + id);
        }

        public void RemoveSubjectFromDB(int idSubject, int idSem1, int idSem2)
        {
            Begin();
            MySqlHelper.Delete("Semester", "idSemester", idSem1);
            MySqlHelper.Delete("Semester", "idSemester", idSem2);
            MySqlHelper.Delete("Subject", "idSubject", idSubject);
            End();
        }
    }
}
