﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudyLoad.DAO
{
    public abstract class DAObject
    {
        private bool _opened;

        public void Begin()
        {
            if(_opened)
                throw new Exception("connection alredy opened");
            _opened = true;
            MySqlHelper.OpenConnection();
        }

        public void End()
        {
            if(!_opened)
                throw new Exception("already closed");
            _opened = false;
            MySqlHelper.CloseConnection();
        }
    }
}
