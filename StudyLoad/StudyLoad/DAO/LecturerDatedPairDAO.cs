﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class LecturerDatedPairDAO : DAObject
    {
        public int InsertToDb(int hours, DateTime dateTime, int idStudyload)
        {
            Begin();
            MySqlHelper.Insert("DatedLecturerPair", new[]
                                                        {
                                                            "Date",
                                                            "Hours",
                                                            "idStudyload"
                                                        },
                               new object[]
                                   {
                                       DateHelper.ToMySqlDate(dateTime),
                                       hours,
                                       idStudyload
                                   });
            var p = MySqlHelper.GetLastInsertId();
            End();
            return p;
        }

        public void UpdateInDb(int id, int hours, DateTime dateTime, int idStudyload)
        {
            Begin();
            MySqlHelper.Update("DatedLecturerPair", new[]
                                                        {
                                                            "Date",
                                                            "Hours"
                                                        },
                               new object[]
                                   {
                                       DateHelper.ToMySqlDate(dateTime),
                                       hours
                                   }, "idDatedLecturerPair = " + id);
            End();
        }

        public void RemoveFromDb(int id)
        {
            Begin();
            MySqlHelper.Delete("DatedLecturerPair", "idDatedLecturerPair", id);
            End();
        }

        public DataTable GetPairsForMonth(int lecturerId, int month, int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery(@"SELECT * FROM DatedLecturerPair dlp, Studyload s WHERE s.idStudyLoad = dlp.idStudyLoad and
                                                                                                            MONTH(dlp.Date) = @m and
                                                                                                            YEAR(dlp.Date) = @y and
                                                                                                            s.idLecturer = @lid",
                                                                                             new []
                                                                                             {
                                                                                                 new MySqlParameter("m", month),
                                                                                                 new MySqlParameter("y", year),
                                                                                                 new MySqlParameter("lid", lecturerId)
                                                                                             });
            End();
            return p;
        }

        public DataTable GetPairsForYear(int lecturerId, int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery(@"SELECT * FROM DatedLecturerPair dlp, Studyload s WHERE s.idStudyLoad = dlp.idStudyLoad and
                                                                                                            YEAR(dlp.Date) = @y and
                                                                                                            s.idLecturer = @lid",
                                                                                             new[]
                                                                                             {
                                                                                                 new MySqlParameter("y", year),
                                                                                                 new MySqlParameter("lid", lecturerId)
                                                                                             });
            End();
            return p;
        }
    }
}
