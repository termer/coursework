﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    class SubjectTypeDAO:DAObject
    {
        public DataTable GetSubjectTypes()
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery("SELECT * FROM SubjectType", new MySqlParameter[] { });
            End();
            return p;
        }

        public int InsertToDB(string name)
        {
            Begin();
            MySqlHelper.Insert("SubjectType", new[]
                                              {
                                                  "subjectTypeName",
                                              },
                                          new object[]
                                              {
                                                  name
                                              });
            var id = MySqlHelper.GetLastInsertId();
            End();
            return id;
        }

        public void UpdateInDB(int id, string name)
        {
            Begin();
            MySqlHelper.Update("SubjectType", new[]
                                               {
                                                   "subjectTypeName",
                                               },
                               new object[]
                                   {
                                       name
                                   }, "idSubjectType = " + id);
            End();
        }
        
        public void RemoveFromDB(int id)
        {
            Begin();
            MySqlHelper.Delete("SubjectType", "idSubjectType", id);
            End();
        }
    }
}
