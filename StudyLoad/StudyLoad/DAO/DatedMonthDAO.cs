﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class DatedMonthDAO:DAObject
    {
        public DataTable GetMonth(int idStudyload, int month)
        {
            Begin();

            var p =
                MySqlHelper.ExecuteSelectQuery(
                    @"SELECT dm.* FROM studyload sl, DatedMonth dm WHERE sl.idStudyload = @slID and
                                                                         dm.Month = @month",
                    new[]
                        {
                            new MySqlParameter("slID", idStudyload),
                            new MySqlParameter("month", month),
                        });

            End();
            return p;
        }

        public int InsertInDb(int idstudyload, int month, int hours)
        {
            Begin();
            MySqlHelper.Insert("DatedMonth", new[]
                                                 {
                                                     "idStudyload",
                                                     "Month",
                                                     "Hours",
                                                 },
                                             new object[]
                                                             {
                                                                 idstudyload,
                                                                 month,
                                                                 hours
                                                             });
            var p = MySqlHelper.GetLastInsertId();
            End();
            return p;
        }

        public void UpdateInDb(int id, int idstudyload, int month, int hours)
        {
            Begin();

            MySqlHelper.Update("DatedMonth", new []
                                                 {
                                                     "idStudyload",
                                                     "Month",
                                                     "Hours",
                                                 },
                                                 new object[]
                                                     {
                                                         idstudyload,
                                                         month,
                                                         hours
                                                     }, "idDatedMonth = " + id);

            End();
        }

        public void RemoveFromDb(int id)
        {
            Begin();
            MySqlHelper.Delete("DatedMonth", "idDatedMonth", id);
            End();
        }
    }
}
