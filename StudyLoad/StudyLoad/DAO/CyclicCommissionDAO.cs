﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class CyclicCommissionDAO:DAObject
    {
        public DataTable GetCyclicCommissions(int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery(@"SELECT * FROM CyclicCommission cc, CyclicCommission_has_Lecturer cchl, Lecturer l 
                                                     WHERE cc.idCyclicCommission = cchl.idCyclicCommission and
                                                           l.idLecturer = cchl.idLecturer", new MySqlParameter[] { });
            End();
            return p;
        }

        public int InsertToDB(string name, int year, int[] ids)
        {
            Begin();
            MySqlHelper.Insert("CyclicCommission", new[]
                                              {
                                                  "Name",
                                                  "Year"
                                              },
                                          new object[]
                                              {
                                                  name,
                                                  year
                                              });
            var id = MySqlHelper.GetLastInsertId();

            InsertIds(id, ids);

            End();
            return id;
        }

        public void UpdateInDB(int id, string name, int year, int[] newids, int[] remids)
        {
            Begin();
            MySqlHelper.Update("CyclicCommission", new[]
                                               {
                                                   "Name",
                                                   "Year"
                                               },
                               new object[]
                                   {
                                       name,
                                       year
                                   }, "idCyclicCommission = " + id);
            InsertIds(id, newids);
            RemoveIds(id, remids);
            End();
        }

        public void RemoveFromDB(int id)
        {
            Begin();
            RemoveAllIds(id);
            MySqlHelper.Delete("CyclicCommission", "idCyclicCommission", id);
            End();
        }

        private void InsertIds(int id, int[] ids)
        {
            foreach (var t in ids)
            {
                MySqlHelper.Insert("CyclicCommission_has_Lecturer", new[]
                                                                        {
                                                                            "idCyclicCommission",
                                                                            "idLecturer"
                                                                        },
                                   new object[]
                                       {
                                           id,
                                           t
                                       });
            }
        }
        private void RemoveIds(int id, int[] ids)
        {
            foreach (var t in ids)
            {
                MySqlHelper.DeleteByCondition("CyclicCommission_has_Lecturer", "idLecturer = " + t + " and idCyclicComission = " + id);
            }
        }

        private void RemoveAllIds(int id)
        {
             MySqlHelper.DeleteByCondition("CyclicCommission_has_Lecturer", "idCyclicComission = " + id);
        }
    }
}
