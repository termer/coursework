﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    class DepartmentDAO:DAObject
    {
        public DataTable GetDepartments(int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery("SELECT * FROM Department d WHERE d.Year =@y", new [] { new MySqlParameter("y", year) });
            End();
            return p;
        }

        public int InsertToDB(string name, int semesterHours, int year)
        {
            Begin();
            MySqlHelper.Insert("Department", new[]
                                              {
                                                  "departmentName",
                                                  "Year",
                                                  "SemesterHours"
                                              },
                                          new object[]
                                              {
                                                  name,
                                                  year,
                                                  semesterHours
                                              });
            var id = MySqlHelper.GetLastInsertId();
            End();
            return id;
        }

        public void UpdateInDB(int id, string name, int semesterHours, int year)
        {
            Begin();
            MySqlHelper.Update("Department", new[]
                                               {
                                                   "departmentName",
                                                   "Year",
                                                  "SemesterHours"
                                               },
                               new object[]
                                   {
                                       name,
                                       year,
                                       semesterHours
                                   }, "idDepartment = " + id);
            End();
        }
        
        public void RemoveFromDB(int id)
        {
            Begin();
            MySqlHelper.Delete("Department", "idDepartment", id);
            End();
        }
    }
}
