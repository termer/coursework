﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class GroupDAO : DAObject
    {
        public DataTable GetGroups(int year)
        {
            Begin();
            var table = MySqlHelper.ExecuteSelectQuery(@"SELECT * FROM studentsgroup sg, department d
                                                         WHERE sg.idDepartment = d.idDepartment and sg.Year = @y", new [] {new MySqlParameter("y", year)});
            End();
            return table;
        }

        public int InsertToDB(string num, int idDepartment, int admissionYear, int budgetCount, int contractCount, int year)
        {
            Begin();
            MySqlHelper.Insert("StudentsGroup", new[]
                                                    {
                                                        "groupNum",
                                                        "idDepartment",
                                                        "admissionYear",
                                                        "contractCount",
                                                        "budgetCount",
                                                        "Year"
                                                    },
                               new object[]
                                   {
                                       num,
                                       idDepartment,
                                       admissionYear,
                                       budgetCount,
                                       contractCount,
                                       year
                                   });
            var p = MySqlHelper.GetLastInsertId();
            End();
            return p;
        }

        public DataTable GetDependableLecturers(int id)
        {
            Begin();

            var table = MySqlHelper.ExecuteSelectQuery(
                @"SELECT * FROM studentsgroup sg, Lecturer l, StudyLoad sl 
                           WHERE sg.idGroup = @id and
                                 sg.idGroup = sl.idGroup and
                                 l.idLecturer = sl.idLecturer
                           GROUP BY sg.idGroup",
                new []
                    {
                        new MySqlParameter("id", id)
                    });



            End();
            return table;
        }

        public DataTable GetGroupWithId(int id)
        {
            Begin();
            var table = MySqlHelper.ExecuteSelectQuery(@"SELECT * FROM studentsgroup sg, department d WHERE sg.idGroup = @id and
                                                                                                            d.idDepartment = sg.idDepartment",
                                                       new[]
                                                           {
                                                               new MySqlParameter("id", id)
                                                           });


            End();
            return table;
        }


        internal void UpdateInDB(int id, string num, int idDepartment, int admissionYear, int budgetCount, int contractCount, int year)
        {
            Begin();

            MySqlHelper.Update("StudentsGroup", new[]
                                                    {
                                                        "groupNum",
                                                        "idDepartment",
                                                        "admissionYear",
                                                        "contractCount",
                                                        "budgetCount",
                                                        "Year"
                                                    },new object[]
                                   {
                                       num,
                                       idDepartment,
                                       admissionYear,
                                       budgetCount,
                                       contractCount,
                                       year
                                   }, "idGroup = " + id);

            End();
        }

        public void RemoveInDB(int id)
        {
            Begin();
            MySqlHelper.Delete("StudentsGroup", "idGroup", id);
            End();
        }
    }

}
