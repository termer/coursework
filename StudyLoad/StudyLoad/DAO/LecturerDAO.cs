﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using StudyLoad.VO;

namespace StudyLoad.DAO
{
    public class LecturerDAO:DAObject
    {
        public DataTable GetLecturers(int year)
        {
            Begin();
            var p = MySqlHelper.ExecuteSelectQuery("SELECT * FROM Lecturer WHERE year = @y", new []{new MySqlParameter("y", year)});
            End();
            return p;
        }

        public int InsertToDb(string FIO, int rate, int year)
        {
            Begin();
            MySqlHelper.Insert("Lecturer", new[]
                                              {
                                                  "FIO",
                                                  "rate",
                                                  "Year"
                                              },
                                          new object[]
                                              {
                                                  FIO,
                                                  rate,
                                                  year
                                              });
            var id =  MySqlHelper.GetLastInsertId();
            End();
            return id;
        }

        public void UpdateInDb(int id, string fio, int rate, int year)
        {
            Begin();
            MySqlHelper.Update("Lecturer", new []
                                               {
                                                   "FIO",
                                                   "rate",
                                                   "Year"
                                               },
                               new object[]
                                   {
                                       fio,
                                       rate,
                                       year
                                   }, "idLecturer = " + id);
            End();
        }

        public void RemoveFromDb(int id)
        {
            Begin();
            MySqlHelper.Delete("Lecturer", "idLecturer", id);

            End();
        }



    }
}
