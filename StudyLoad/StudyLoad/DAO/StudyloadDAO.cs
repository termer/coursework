﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace StudyLoad.DAO
{
    public class StudyloadDAO:DAObject
    {
        public int InsertToDB(int lecturerId, int groupId, int subjectId, int year)
        {
            Begin();
            MySqlHelper.Insert("StudyLoad", new []
                                                {
                                                    "idLecturer",
                                                    "idGroup",
                                                    "idSubject",
                                                    "Year"
                                                }, 
                                            new object[]
                                                {
                                                    lecturerId,
                                                    groupId,
                                                    subjectId,
                                                    year
                                                });
            var id = MySqlHelper.GetLastInsertId();
            End();
            return id;
        }

        public void UpdateInDb(int id, int lecturerId, int subjectId, int groupId, int year)
        {
            Begin();
            MySqlHelper.Update("StudyLoad", new []
                                                {
                                                    "idGroup",
                                                    "idLecturer",
                                                    "idSubject",
                                                    "Year"
                                                },
                                            new object[]
                                                {
                                                    lecturerId,
                                                    subjectId,
                                                    groupId,
                                                    year
                                                }, "idStudyLoad = " + id);
            End();
        }

        public DataTable GetSubjectsForLecturer(int idLecturer, int year)
        {
            Begin();
            var p =
                MySqlHelper.ExecuteSelectQuery(
                    "SELECT * FROM Studyload sl WHERE sl.idLecturer = @idLec and sl.Year = @year",
                    new[]
                        {
                            new MySqlParameter("idLec", idLecturer),
                            new MySqlParameter("year", year)
                        });
            End();
            return p;
        }

        public DataTable GetBindedSubjectsForSubject(int id)
        {
            Begin();

            var table = MySqlHelper.ExecuteSelectQuery(
                @"SELECT * FROM Lecturer l, StudyLoad sl, Subject s 
                           WHERE s.idSubject = @id and
                                 l.idLecturer = sl.idLecturer",
                new[]
                    {
                        new MySqlParameter("id", id)
                    });
            End();
            return table;
        }


        public DataTable GetSubjectsForGroup(int idGroup)
        {
            Begin();
            var p =
                MySqlHelper.ExecuteSelectQuery(
                    "SELECT * FROM Studyload sl WHERE sl.idGroup = @idGr",
                    new[]
                        {
                            new MySqlParameter("idGr", idGroup)
                        });
            End();
            return p;
        }

        public void RemoveFromDb(int id)
        {
            Begin();
            MySqlHelper.DeleteByCondition("datedLecturerPair", "idStudyload = " + id );
            MySqlHelper.Delete("Studyload", "idStudyload", id);
            End();
        }


        public DataTable GetPairs(int idStudyLoad)
        {
            Begin();
            var p =
                MySqlHelper.ExecuteSelectQuery(
                    @"SELECT * FROM Studyload sl, LecturerPair lp WHERE 
                                sl.idStudyLoad = @idLoad and
                                lp.idStudyLoad = @idLoad",
                    new[]
                        {
                            new MySqlParameter("idLoad", idStudyLoad),
                        });
            End();
            return p;
        }

        public int InsertPair(int idStudyload, int day, int semester, int hours, int type)
        {
            Begin();
            MySqlHelper.Insert("LecturerPair", new[]
                                                {
                                                    "idStudyLoad",
                                                    "Hours",
                                                    "Semester",
                                                    "Day",
                                                    "WeekType"
                                                },
                                            new object[]
                                                {
                                                    idStudyload,
                                                    hours,
                                                    semester,
                                                    day,
                                                    type
                                                });
            var id = MySqlHelper.GetLastInsertId();
            End();
            return id;
        }
        public int UpdatePair(int idPair, int idStudyload, int day, int semester, int hours, int type)
        {
            Begin();
            MySqlHelper.Insert("StudyLoad", new[]
                                                {
                                                    "idPair",
                                                    "idStudyLoad",
                                                    "Hours",
                                                    "Semester",
                                                    "Day",
                                                    "type"
                                                },
                                            new object[]
                                                {
                                                    idStudyload,
                                                    hours,
                                                    semester,
                                                    day,
                                                    type
                                                });
            var id = MySqlHelper.GetLastInsertId();
            End();
            return id;
        }

        internal void RemovePairFromDb(int id)
        {
            Begin();
            MySqlHelper.Delete("LecturerPair", "idLecturerPair", id);
            End();
        }
    }
}
