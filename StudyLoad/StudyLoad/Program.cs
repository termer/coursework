﻿using System;
using System.Windows.Forms;
using StudyLoad.Forms;
using StudyLoad.VO;

namespace StudyLoad
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {

            try
            {            
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            finally
            {
                MySqlHelper.DisposeConnections();
            }
        }
    }
}
