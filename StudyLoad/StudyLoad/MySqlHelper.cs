﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;



namespace StudyLoad
{
    public static class MySqlHelper
    {

        private static MySqlConnection _serverConnection;
        private static MySqlConnection _databaseConnection;

        private static MySqlDataAdapter _dataAdapter;

        private static string _serverConnectionString;
        private static string _databaseConnectionString;

        static MySqlHelper()
        {
        }

        public static void SetConnection(string serverName, string userName, string password)
        {
            _serverConnectionString = "server=" + serverName + ";user=" + userName + ";port=3306;password=" + password + ";";
            _databaseConnectionString = _serverConnectionString + "database=studyload";

            _serverConnection = new MySqlConnection(_serverConnectionString);
            _databaseConnection = new MySqlConnection(_databaseConnectionString);

            _serverConnection.Open();
            var ping = _serverConnection.Ping();
            if(!ping)
                throw new Exception("Connection to database failed");

            _serverConnection.Close();
        }





        public static void InitDatabase()
        {
            try
            {
                _serverConnection.Open();

                var cmd = CreateMySqlCommand( "CALL PROCEDURE ");//todo:
                cmd.ExecuteNonQuery();

                _serverConnection.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void OpenConnection()
        {
            try
            {
                _databaseConnection.Open();
            }
            catch (MySqlException e)
            {
                Debug.WriteLine("Could not open connection: " + e.ToString());
            }
        }

        public static void CloseConnection()
        {
            _databaseConnection.Close();
        }

        public static int GetLastInsertId()
        {
            var command = CreateMySqlCommand("SELECT LAST_INSERT_ID()");

            using(var reader = command.ExecuteReader())
            {
                if (reader != null && reader.Read())
                    return reader.GetInt32(0);
            }
            throw new Exception("Something went wrong...");
        }

        public static DataTable CallProcedure(string procedure, MySqlParameter[] parameters)
        {
            var command = CreateMySqlCommand(procedure);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            _dataAdapter = new MySqlDataAdapter(command);
            _dataAdapter.SelectCommand = command;

            var dataSet = new DataSet();
            _dataAdapter.Fill(dataSet);

            return dataSet.Tables[0]; 
        }


        public static DataTable ExecuteSelectQuery(string query, MySqlParameter[] parameters)
        {
            var command = CreateMySqlCommand(query);
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            _dataAdapter = new MySqlDataAdapter(command);
            _dataAdapter.SelectCommand = command;

            var dataSet = new DataSet();
            _dataAdapter.Fill(dataSet);

            var dataTable = dataSet.Tables[0]; 

            return dataTable;
        }



        public static void Insert(string table, string[] @params, object[] values)
        {
            int length = @params.Length;

            Debug.Assert(length == values.Length, "Wrong params/values length");


            var paramsString = "";
            for (int i = 0; i < length; i++)
            {
                paramsString += @params[i];
                if(i != length - 1)
                    paramsString += ",";
            }
            
            var valuesString = "";
            for (int i = 0; i < length; i++)
            {
                valuesString += "@" + @params[i];
                if(i != length - 1)
                    valuesString += ",";
            }

            var command = CreateMySqlCommand("INSERT INTO " + table + "(" + paramsString + ") VALUES(" + valuesString + ")");

            for (int i = 0; i < length; i++)
                command.Parameters.AddWithValue("@" + @params[i], values[i].ToString());

            command.ExecuteNonQuery();
        }

        public static void Delete(string table, string idname, int id)
        {
            var command = CreateMySqlCommand("DELETE FROM " + table + " WHERE " + idname + " = " + id);
            command.ExecuteNonQuery();
        }

        public static void DeleteByCondition(string table, string condition)
        {
            var command = CreateMySqlCommand("DELETE FROM " + table + " WHERE " + condition);
            command.ExecuteNonQuery();
        }

        public static void Update(string table, string[] @params, object[] values, string condition)
        {
            int length = @params.Length;
            Debug.Assert(length == values.Length, "Wrong params/values length");

            var paramValuesString = "";
            for (int i = 0; i < length; i++)
            {
                paramValuesString += @params[i] + "=" + "@" + @params[i] + "";
                if(i != length - 1)
                    paramValuesString += ",";
            }

            var command = CreateMySqlCommand("UPDATE " + table + " SET " + paramValuesString + " WHERE " + condition);
            for (int i = 0; i < length; i++)
                command.Parameters.AddWithValue(@params[i], values[i].ToString());
            command.ExecuteNonQuery();
        }


        public static MySqlDataAdapter GetDataAdapter(string query)
        {
            _databaseConnection.Open();
            var dataAdapter = new MySqlDataAdapter(query, _databaseConnection);
            var commandBuilder = new MySqlCommandBuilder();

            dataAdapter.InsertCommand = commandBuilder.GetInsertCommand();
            dataAdapter.DeleteCommand = commandBuilder.GetDeleteCommand();
            dataAdapter.UpdateCommand = commandBuilder.GetUpdateCommand();

            _databaseConnection.Close();
            
            return dataAdapter;
        }

        public static void DisposeConnections()
        {
            _databaseConnection.Close();
            _serverConnection.Close();

            _databaseConnection.Dispose();
            _serverConnection.Dispose();

            _databaseConnection = null;
            _serverConnection = null;
        }

        private static MySqlCommand CreateMySqlCommand(string query)
        {
            var command = new MySqlCommand
            {
                Connection = _databaseConnection,
                CommandText = query
            };
            return command;
        }
    }
}
