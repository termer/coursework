﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StudyLoad
{
    public enum DayOfWeek
    {
        Monday = 1,
        Tuesday = 2,
        Wednsday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7,
        None = 8
    }

    public enum Month
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12,
        None = 13
    }

    public static class DateHelper
    {
        static Dictionary<DayOfWeek, string> _dayNamesEN;
        static Dictionary<DayOfWeek, string> _dayNamesRU;

        static Dictionary<Month, string> _monthNamesRU;

        static DateHelper()
        {
            _dayNamesEN = new Dictionary<DayOfWeek, string>(8);
            _dayNamesRU = new Dictionary<DayOfWeek, string>(8);
            _monthNamesRU = new Dictionary<Month, string>(16);

            _dayNamesEN.Add(DayOfWeek.Sunday, "Sunday");
            _dayNamesEN.Add(DayOfWeek.Monday, "Monday");
            _dayNamesEN.Add(DayOfWeek.Tuesday, "Tuesday");
            _dayNamesEN.Add(DayOfWeek.Wednsday, "Wednsday");
            _dayNamesEN.Add(DayOfWeek.Thursday, "Thursday");
            _dayNamesEN.Add(DayOfWeek.Friday, "Friday");
            _dayNamesEN.Add(DayOfWeek.Saturday, "Saturday");


            _dayNamesRU.Add(DayOfWeek.Sunday, "вс");
            _dayNamesRU.Add(DayOfWeek.Monday, "пн");
            _dayNamesRU.Add(DayOfWeek.Tuesday, "вт");
            _dayNamesRU.Add(DayOfWeek.Wednsday, "ср");
            _dayNamesRU.Add(DayOfWeek.Thursday, "чт");
            _dayNamesRU.Add(DayOfWeek.Friday, "пт");
            _dayNamesRU.Add(DayOfWeek.Saturday, "сб");

            _monthNamesRU.Add(Month.January, "Январь");
            _monthNamesRU.Add(Month.February, "Февраль");
            _monthNamesRU.Add(Month.March, "Март");
            _monthNamesRU.Add(Month.April, "Апрель");
            _monthNamesRU.Add(Month.May, "Май");
            _monthNamesRU.Add(Month.June, "Июнь");
            _monthNamesRU.Add(Month.July, "Июль");
            _monthNamesRU.Add(Month.August, "Август");
            _monthNamesRU.Add(Month.September, "Сентябрь");
            _monthNamesRU.Add(Month.October, "Октябрь");
            _monthNamesRU.Add(Month.November, "Ноябрь");
            _monthNamesRU.Add(Month.December, "Декабрь");
        }

        static public string GetDayName(DayOfWeek day, bool russian)
        {
            return russian
                       ? _dayNamesRU[day]
                       : _dayNamesEN[day];
        }

        static public string ToMySqlDate(DateTime dateTime)
        {
            //string year = dateTime.Year.ToString();
            //string month = dateTime.Month.ToString();
            //string day = dateTime.Day.ToString();

            //if (day.Count() < 2)
            //    day = day.Insert(0, "0");

            //return  year + "-" + month + '-' + day;

            return dateTime.ToString("yyyy-MM-dd");
        }
        static public string GetMonthName(Month month, bool russian)
        {
            return russian
                       ? _monthNamesRU[month]
                       : month.ToString();
        }
    }
}
