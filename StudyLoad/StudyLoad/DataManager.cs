﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using StudyLoad.BUS;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;
using System.Xml;
using System.Xml.Serialization;

namespace StudyLoad
{
    public class DataManager<T> where T : IVObject
    {
        public readonly List<T> Data;

        public DataManager()
        {
            Data = new List<T>();
        }

        public void Add(T obj)
        {
            Data.Add(obj);
        }

        internal void Add(T[] objects)
        {
            Data.AddRange(objects);
        }


        public void Remove(T obj)
        {
            T data = Data.First(p => p.Equals(obj));

            if (data.Status == DataStatus.NotChanged || data.Status == DataStatus.Updated)
                data.Status = DataStatus.Removed;

            if (data.Status == DataStatus.New)
                Data.Remove(data);
        }

        public bool IsDataChanged()
        {
            return
                Data.Any(
                    p => p.Status == DataStatus.New || 
                         p.Status == DataStatus.Updated || 
                         p.Status == DataStatus.Removed);
        }

        public void Update(IBUSObject<T> busObject, int year)
        {
            var _deleteQueue = new Queue<T>();

            foreach (var data in Data)
            {
                if (busObject.UpdateInDataBase(data, year) == -1)
                    _deleteQueue.Enqueue(data);
            }


            while (_deleteQueue.Count > 0)
            {
                T gameObject = _deleteQueue.Dequeue();
                Data.Remove(gameObject);
            }
        }

        public void Serialize(string path, string filename)
        {
            var s = new XmlSerializer(Data.GetType(), new XmlAttributeOverrides());
            Directory.CreateDirectory(path);
            using (var stream = new FileStream(path + filename, FileMode.OpenOrCreate))
            {
                s.Serialize(stream, Data);
            }
        }


    }
}
