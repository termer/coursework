﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.VO;
using StudyLoad.Forms.EditForms;

namespace StudyLoad.Forms
{
    public partial class EditDepartmentForm : ErrorForm
    {
        public DepartmentVO Department;

        public EditDepartmentForm(DepartmentVO department = null)
        {
            InitializeComponent();

            if (department == null)
            {
                Department = new DepartmentVO();
                return;
            }
            Department = department;
            nameText.Text = department.Name;
            hoursNumeric.Value = department.SemesterHours;
        }


        private void okButton_Click(object sender, EventArgs e)
        {


            bool ok = true;
            if (nameText.Text == "")
            {
                ShowError(nameText, "Пустое имя");
                ok = false;
            }
            else
            {
                RemoveError(nameText);
            }

            if (hoursNumeric.Value == 0)
            {
                ShowError(hoursNumeric, "Не указано количество часов");
                ok = false;
            }
            else
            {
                RemoveError(hoursNumeric);
            }

            if (!ok)
            {
                return;
            }

            Department.Name = nameText.Text;
            Department.SemesterHours = (int)hoursNumeric.Value;

            DialogResult = DialogResult.OK;

            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}
