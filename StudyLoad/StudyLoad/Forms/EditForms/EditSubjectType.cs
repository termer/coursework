﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.VO;
using StudyLoad.Forms.EditForms;

namespace StudyLoad.Forms
{
    public partial class EditSubjectType : ErrorForm
    {
        public SubjectTypeVO Subject;

        public EditSubjectType(SubjectTypeVO subject = null)
        {
            InitializeComponent();

            if (subject == null)
            {
                Subject = new SubjectTypeVO();
                return;
            }
            Subject = subject;
            nameText.Text = subject.Name;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            bool ok = true;
            if (String.IsNullOrEmpty(nameText.Text))
            {
                ShowError(nameText, "Пустое имя");
                ok = false;
            }

            if (!ok)
            {
                return;
            }

            DialogResult = DialogResult.OK;
            Subject.Name = nameText.Text;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}
