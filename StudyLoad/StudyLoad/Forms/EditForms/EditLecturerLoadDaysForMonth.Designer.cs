﻿namespace StudyLoad.Forms
{
    partial class EditLecturerLoadDaysForMonth
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.subjectsGroupsListView = new System.Windows.Forms.ListView();
            this.nameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rateHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lecNameLabel = new System.Windows.Forms.Label();
            this.monthLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.monthListView = new StudyLoad.Controls.EditableList();
            this.overallListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // subjectsGroupsListView
            // 
            this.subjectsGroupsListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.subjectsGroupsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.rateHeader});
            this.subjectsGroupsListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.subjectsGroupsListView.FullRowSelect = true;
            this.subjectsGroupsListView.GridLines = true;
            this.subjectsGroupsListView.Location = new System.Drawing.Point(16, 31);
            this.subjectsGroupsListView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.subjectsGroupsListView.MultiSelect = false;
            this.subjectsGroupsListView.Name = "subjectsGroupsListView";
            this.subjectsGroupsListView.Size = new System.Drawing.Size(212, 428);
            this.subjectsGroupsListView.TabIndex = 1;
            this.subjectsGroupsListView.UseCompatibleStateImageBehavior = false;
            this.subjectsGroupsListView.View = System.Windows.Forms.View.Details;
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "Предмет";
            this.nameHeader.Width = 137;
            // 
            // rateHeader
            // 
            this.rateHeader.Text = "Группа";
            this.rateHeader.Width = 70;
            // 
            // lecNameLabel
            // 
            this.lecNameLabel.AutoSize = true;
            this.lecNameLabel.Location = new System.Drawing.Point(6, 6);
            this.lecNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lecNameLabel.Name = "lecNameLabel";
            this.lecNameLabel.Size = new System.Drawing.Size(86, 20);
            this.lecNameLabel.TabIndex = 3;
            this.lecNameLabel.Text = "LECNAME";
            // 
            // monthLabel
            // 
            this.monthLabel.AutoSize = true;
            this.monthLabel.Location = new System.Drawing.Point(235, 6);
            this.monthLabel.Name = "monthLabel";
            this.monthLabel.Size = new System.Drawing.Size(66, 20);
            this.monthLabel.TabIndex = 4;
            this.monthLabel.Text = "MONTH";
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.Location = new System.Drawing.Point(1135, 475);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(252, 47);
            this.okButton.TabIndex = 5;
            this.okButton.Text = "ок";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelButton.Location = new System.Drawing.Point(15, 475);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(213, 47);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // monthListView
            // 
            this.monthListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.monthListView.FullRowSelect = true;
            this.monthListView.GridLines = true;
            this.monthListView.Location = new System.Drawing.Point(245, 31);
            this.monthListView.Name = "monthListView";
            this.monthListView.Size = new System.Drawing.Size(884, 428);
            this.monthListView.TabIndex = 0;
            this.monthListView.UseCompatibleStateImageBehavior = false;
            this.monthListView.View = System.Windows.Forms.View.Details;
            // 
            // overallListView
            // 
            this.overallListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.overallListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.overallListView.Location = new System.Drawing.Point(1135, 31);
            this.overallListView.Name = "overallListView";
            this.overallListView.Size = new System.Drawing.Size(252, 427);
            this.overallListView.TabIndex = 7;
            this.overallListView.UseCompatibleStateImageBehavior = false;
            this.overallListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Распределенно";
            this.columnHeader1.Width = 133;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Всего";
            this.columnHeader2.Width = 64;
            // 
            // EditLecturerLoadDaysForMonth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1399, 541);
            this.Controls.Add(this.overallListView);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.monthListView);
            this.Controls.Add(this.monthLabel);
            this.Controls.Add(this.lecNameLabel);
            this.Controls.Add(this.subjectsGroupsListView);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "EditLecturerLoadDaysForMonth";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView subjectsGroupsListView;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader rateHeader;
        private System.Windows.Forms.Label lecNameLabel;
        private System.Windows.Forms.Label monthLabel;
        private Controls.EditableList monthListView;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ListView overallListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}
