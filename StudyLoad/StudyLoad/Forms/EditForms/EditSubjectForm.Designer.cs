﻿namespace StudyLoad
{
    partial class EditSubjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subjName = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.Lectures = new System.Windows.Forms.Label();
            this.lecNum = new System.Windows.Forms.NumericUpDown();
            this.labNum = new System.Windows.Forms.NumericUpDown();
            this.Labs = new System.Windows.Forms.Label();
            this.courseNum = new System.Windows.Forms.NumericUpDown();
            this.CourseWork = new System.Windows.Forms.Label();
            this.consultNum = new System.Windows.Forms.NumericUpDown();
            this.Consultation = new System.Windows.Forms.Label();
            this.examNum = new System.Windows.Forms.NumericUpDown();
            this.Exam = new System.Windows.Forms.Label();
            this.creditNum = new System.Windows.Forms.NumericUpDown();
            this.Credit = new System.Windows.Forms.Label();
            this.codeText = new System.Windows.Forms.TextBox();
            this.Code = new System.Windows.Forms.Label();
            this.Sum = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Semester = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.weekTimeNumeric = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.overallNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lecNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.examNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekTimeNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.overallNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // subjName
            // 
            this.subjName.AutoSize = true;
            this.subjName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjName.Location = new System.Drawing.Point(23, 10);
            this.subjName.Name = "subjName";
            this.subjName.Size = new System.Drawing.Size(40, 20);
            this.subjName.TabIndex = 0;
            this.subjName.Text = "Имя";
            this.subjName.Click += new System.EventHandler(this.label1_Click);
            // 
            // nameText
            // 
            this.nameText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameText.Location = new System.Drawing.Point(191, 10);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(140, 26);
            this.nameText.TabIndex = 1;
            // 
            // Lectures
            // 
            this.Lectures.AutoSize = true;
            this.Lectures.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lectures.Location = new System.Drawing.Point(23, 269);
            this.Lectures.Name = "Lectures";
            this.Lectures.Size = new System.Drawing.Size(65, 20);
            this.Lectures.TabIndex = 2;
            this.Lectures.Text = "Лекции";
            // 
            // lecNum
            // 
            this.lecNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lecNum.Location = new System.Drawing.Point(191, 269);
            this.lecNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.lecNum.Name = "lecNum";
            this.lecNum.Size = new System.Drawing.Size(140, 26);
            this.lecNum.TabIndex = 4;
            this.lecNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // labNum
            // 
            this.labNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNum.Location = new System.Drawing.Point(191, 301);
            this.labNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.labNum.Name = "labNum";
            this.labNum.Size = new System.Drawing.Size(140, 26);
            this.labNum.TabIndex = 5;
            this.labNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // Labs
            // 
            this.Labs.AutoSize = true;
            this.Labs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Labs.Location = new System.Drawing.Point(23, 301);
            this.Labs.Name = "Labs";
            this.Labs.Size = new System.Drawing.Size(122, 20);
            this.Labs.TabIndex = 4;
            this.Labs.Text = "Лабораторные";
            // 
            // courseNum
            // 
            this.courseNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courseNum.Location = new System.Drawing.Point(191, 333);
            this.courseNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.courseNum.Name = "courseNum";
            this.courseNum.Size = new System.Drawing.Size(140, 26);
            this.courseNum.TabIndex = 6;
            this.courseNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // CourseWork
            // 
            this.CourseWork.AutoSize = true;
            this.CourseWork.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CourseWork.Location = new System.Drawing.Point(23, 333);
            this.CourseWork.Name = "CourseWork";
            this.CourseWork.Size = new System.Drawing.Size(137, 20);
            this.CourseWork.TabIndex = 6;
            this.CourseWork.Text = "Курсовая работа";
            // 
            // consultNum
            // 
            this.consultNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consultNum.Location = new System.Drawing.Point(191, 365);
            this.consultNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.consultNum.Name = "consultNum";
            this.consultNum.Size = new System.Drawing.Size(140, 26);
            this.consultNum.TabIndex = 7;
            this.consultNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // Consultation
            // 
            this.Consultation.AutoSize = true;
            this.Consultation.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consultation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Consultation.Location = new System.Drawing.Point(23, 365);
            this.Consultation.Name = "Consultation";
            this.Consultation.Size = new System.Drawing.Size(116, 20);
            this.Consultation.TabIndex = 8;
            this.Consultation.Text = "Консультации";
            // 
            // examNum
            // 
            this.examNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.examNum.Location = new System.Drawing.Point(191, 397);
            this.examNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.examNum.Name = "examNum";
            this.examNum.Size = new System.Drawing.Size(140, 26);
            this.examNum.TabIndex = 8;
            this.examNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // Exam
            // 
            this.Exam.AutoSize = true;
            this.Exam.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exam.Location = new System.Drawing.Point(23, 397);
            this.Exam.Name = "Exam";
            this.Exam.Size = new System.Drawing.Size(75, 20);
            this.Exam.TabIndex = 10;
            this.Exam.Text = "Экзамен";
            // 
            // creditNum
            // 
            this.creditNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditNum.Location = new System.Drawing.Point(191, 429);
            this.creditNum.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.creditNum.Name = "creditNum";
            this.creditNum.Size = new System.Drawing.Size(140, 26);
            this.creditNum.TabIndex = 9;
            this.creditNum.ValueChanged += new System.EventHandler(this.numericUpDown6_ValueChanged);
            // 
            // Credit
            // 
            this.Credit.AutoSize = true;
            this.Credit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Credit.Location = new System.Drawing.Point(23, 429);
            this.Credit.Name = "Credit";
            this.Credit.Size = new System.Drawing.Size(67, 20);
            this.Credit.TabIndex = 12;
            this.Credit.Text = "Зачеты";
            // 
            // codeText
            // 
            this.codeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeText.Location = new System.Drawing.Point(191, 92);
            this.codeText.Name = "codeText";
            this.codeText.Size = new System.Drawing.Size(140, 26);
            this.codeText.TabIndex = 2;
            // 
            // Code
            // 
            this.Code.AutoSize = true;
            this.Code.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code.Location = new System.Drawing.Point(21, 92);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(39, 20);
            this.Code.TabIndex = 16;
            this.Code.Text = "Код";
            // 
            // Sum
            // 
            this.Sum.AutoSize = true;
            this.Sum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sum.Location = new System.Drawing.Point(21, 475);
            this.Sum.Name = "Sum";
            this.Sum.Size = new System.Drawing.Size(105, 20);
            this.Sum.TabIndex = 18;
            this.Sum.Text = "Всего часов:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(330, 518);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 32);
            this.button1.TabIndex = 19;
            this.button1.Text = "Принять";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Ok_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(27, 518);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 32);
            this.button2.TabIndex = 20;
            this.button2.Text = "Отменить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Semester
            // 
            this.Semester.AutoSize = true;
            this.Semester.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Semester.Location = new System.Drawing.Point(23, 137);
            this.Semester.Name = "Semester";
            this.Semester.Size = new System.Drawing.Size(75, 20);
            this.Semester.TabIndex = 21;
            this.Semester.Text = "Семестр";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown1.Location = new System.Drawing.Point(191, 137);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(33, 26);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // weekTimeNumeric
            // 
            this.weekTimeNumeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weekTimeNumeric.Location = new System.Drawing.Point(191, 214);
            this.weekTimeNumeric.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.weekTimeNumeric.Name = "weekTimeNumeric";
            this.weekTimeNumeric.Size = new System.Drawing.Size(140, 26);
            this.weekTimeNumeric.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Часы в неделю";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(21, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 20);
            this.label5.TabIndex = 25;
            this.label5.Text = "Тип";
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(191, 58);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 28);
            this.comboBox1.Sorted = true;
            this.comboBox1.TabIndex = 1;
            // 
            // overallNumeric
            // 
            this.overallNumeric.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overallNumeric.Location = new System.Drawing.Point(191, 182);
            this.overallNumeric.Maximum = new decimal(new int[] {
            32000,
            0,
            0,
            0});
            this.overallNumeric.Name = "overallNumeric";
            this.overallNumeric.Size = new System.Drawing.Size(140, 26);
            this.overallNumeric.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Всего часов";
            // 
            // EditSubjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 562);
            this.Controls.Add(this.overallNumeric);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.weekTimeNumeric);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.Semester);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Sum);
            this.Controls.Add(this.codeText);
            this.Controls.Add(this.Code);
            this.Controls.Add(this.creditNum);
            this.Controls.Add(this.Credit);
            this.Controls.Add(this.examNum);
            this.Controls.Add(this.Exam);
            this.Controls.Add(this.consultNum);
            this.Controls.Add(this.Consultation);
            this.Controls.Add(this.courseNum);
            this.Controls.Add(this.CourseWork);
            this.Controls.Add(this.labNum);
            this.Controls.Add(this.Labs);
            this.Controls.Add(this.lecNum);
            this.Controls.Add(this.Lectures);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.subjName);
            this.Name = "EditSubjectForm";
            this.Text = "Редактирование предмета";
            this.Load += new System.EventHandler(this.EditSubjectForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lecNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.examNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekTimeNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.overallNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label subjName;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Label Lectures;
        private System.Windows.Forms.NumericUpDown lecNum;
        private System.Windows.Forms.NumericUpDown labNum;
        private System.Windows.Forms.Label Labs;
        private System.Windows.Forms.NumericUpDown courseNum;
        private System.Windows.Forms.Label CourseWork;
        private System.Windows.Forms.NumericUpDown consultNum;
        private System.Windows.Forms.Label Consultation;
        private System.Windows.Forms.NumericUpDown examNum;
        private System.Windows.Forms.Label Exam;
        private System.Windows.Forms.NumericUpDown creditNum;
        private System.Windows.Forms.Label Credit;
        private System.Windows.Forms.TextBox codeText;
        private System.Windows.Forms.Label Code;
        private System.Windows.Forms.Label Sum;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label Semester;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.NumericUpDown weekTimeNumeric;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown overallNumeric;
        private System.Windows.Forms.Label label2;
    }
}