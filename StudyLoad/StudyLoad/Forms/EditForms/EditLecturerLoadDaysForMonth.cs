﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class EditLecturerLoadDaysForMonth : Form
    {
        private class SubjectListItem : ListViewItem
        {
            public BindedSubjectVO BindedSubject;
            public SubjectListItem(string[] strings)
                : base(strings)
            {
            }
        }
        private class OverallListItem : ListViewItem
        {
            public BindedSubjectVO BindedSubject;
            public OverallListItem(string[] strings)
                : base(strings)
            {
            }
        }


        private readonly LecturerVO _lecturer;
        private readonly AppSettings _settings;
        private readonly Month _currentMonth;

        private readonly LecturerDatedPairBUS _lecturerDatedPairBUS;
        private readonly DataManager<LecturerDatedPairsVO> _pairs;

        public EditLecturerLoadDaysForMonth(LecturerVO lecturer,
                                            AppSettings settings,
                                            Month month = Month.September)
        {
            _lecturer = lecturer;
            _settings = settings;
            _currentMonth = month;

            InitializeComponent();

            GenerateCalendarListView();


            _lecturerDatedPairBUS = new LecturerDatedPairBUS(new StudyloadBUS());

            _pairs = new DataManager<LecturerDatedPairsVO>();

            monthLabel.Text = DateHelper.GetMonthName(month, true);
            lecNameLabel.Text = lecturer.FIO;
            monthListView.ElementChanged += MonthListViewOnElementChanged;


            UpdateData();   
        }

        private void MonthListViewOnElementChanged(object sender, EventArgs e)
        {
            UpdateOverallItems();
        }

        private void GenerateCalendarListView()
        {
            monthListView.Clear();

            var daysCount = DateTime.DaysInMonth(_settings.Year, (int)_currentMonth);

            for (int i = 1; i <= daysCount; i++)
            {
                var header = new ColumnHeader
                                 {
                                     Width = 32,
                                     Text = i.ToString()
                                 };

                monthListView.Columns.Add(header);
            }
        }

        public void UpdateData()
        {
            _pairs.Add(_lecturerDatedPairBUS.GetDatedPairsForMonth(_lecturer, _currentMonth, _settings.Year));
            ListViewItem listitem = null;
            var studyLoad = new StudyloadBUS();
            foreach (var subjectVO in _lecturer.BindedSubjects.Data)
            {

                subjectsGroupsListView.Items.Add(new SubjectListItem(new[]
                                                                      {
                                                                          subjectVO.Subject.Name,
                                                                          subjectVO.Group.FullName
                                                                      }){BindedSubject = subjectVO});
                overallListView.Items.Add(new OverallListItem(new []
                                                                  {
                                                                      "0",
                                                                      studyLoad.CalculateTimeForMonth(_settings.Year, _currentMonth, subjectVO).ToString()
                                                                  }){BindedSubject = subjectVO});

                if (_pairs.Data.Any(p => p.BindedSubjectVO == subjectVO))
                {
                    foreach (var pair in _pairs.Data.Where(p => p.BindedSubjectVO == subjectVO))
                    {
                        var daysCount = DateTime.DaysInMonth(_settings.Year, (int) _currentMonth);

                        for (int i = 1; i <= daysCount; i++)
                        {
                            var j = pair.Pairs.FindIndex(p => p.Date.Day == i);

                            listitem = AddListViewItem(listitem, i,
                                                       j == -1 ?
                                                       0.ToString() :
                                                       pair.Pairs[j].Hours.ToString());
                        }
                    }
                }
                else
                {
                    var daysCount = DateTime.DaysInMonth(_settings.Year, (int)_currentMonth);
                    for (int i = 1; i <= daysCount; i++)
                        listitem = AddListViewItem(listitem, i, 0.ToString());
                }
              
                monthListView.Items.Add(listitem);

                listitem = null;
            }
        }

        private static ListViewItem AddListViewItem(ListViewItem listitem, int i, string value)
        {
            if (i == 1)
                listitem = new ListViewItem(value);
            else
                listitem.SubItems.Add(value);
            return listitem;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            
            for (int i = 0; i < monthListView.Items.Count; i++)
            {
                ListViewItem item = monthListView.Items[i];
                int t = 1;
                foreach (ListViewItem.ListViewSubItem item2 in item.SubItems)
                {
                    var subject = (subjectsGroupsListView.Items[i] as SubjectListItem).BindedSubject;
                    var pair = _pairs.Data.FirstOrDefault(p => p.BindedSubjectVO == subject);

                    int hours = int.Parse(item2.Text);

                    if(hours != 0)
                        if(pair == null)
                        {
                            pair = new LecturerDatedPairsVO(subject);
                            _pairs.Add(pair);
                        }

                    var date = new DateTime(_settings.Year, (int) _currentMonth, t);
                    if(pair != null)
                        if(pair.Pairs.FindIndex(p=> p.Date == date) != -1 || hours != 0)
                            pair.SetPair(hours, date);

                    t++;
                }
            }

            _pairs.Update(_lecturerDatedPairBUS, _settings.Year);
            Close();
        }

        private void UpdateOverallItems()
        {
            for (int i = 0; i < overallListView.Items.Count; i++)
            {
                var item = overallListView.Items[i];
                var item2 = monthListView.Items[i];
                int sum = 0;
                foreach (ListViewItem.ListViewSubItem item3 in item2.SubItems)
                {
                    sum += int.Parse(item3.Text);
                }


                item.SubItems[0].Text = sum.ToString();
            }
        }

    }
}
