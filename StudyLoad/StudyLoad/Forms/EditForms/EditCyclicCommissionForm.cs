﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class EditCyclicCommissionForm : Form
    {
        private class LecturerListItem : ListViewItem
        {
            public LecturerVO Lecturer;
            public LecturerListItem(string[] strings)
                : base(strings)
            {

            }
        }

        private class LecComboBoxItem
        {
            public LecturerVO Lecturer;
            public override string ToString()
            {
                return Lecturer.FIO;
            }
        }

        public CyclicCommissionVO CyclicCommission
        {
            get
            {
                return _cyclicCommission;
            }
        }
        private CyclicCommissionVO _cyclicCommission;


        private readonly AppSettings _settings;

        private List<LecturerVO> _lecturers;

        public EditCyclicCommissionForm(CyclicCommissionVO cyclicCommission, AppSettings settings)
        {
            _cyclicCommission = cyclicCommission;
            _settings = settings;
             _lecturers = new List<LecturerVO>();


            InitializeComponent();



            var lecturers = new LecturerBUS().GetLecturers(_settings.Year);

            if (_cyclicCommission == null)
            {
                _cyclicCommission = new CyclicCommissionVO();
            }
            else
            {
                _lecturers.AddRange(_cyclicCommission.Entries.Select(p => p.Lecturer).ToArray());
                textBox1.Text = _cyclicCommission.Name;
            }

            foreach (var lecturerVO in lecturers)
            {
                lecComboBox.Items.Add(new LecComboBoxItem { Lecturer = lecturerVO });
            }


            UpdateView();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            foreach (var lecturer in _lecturers)
            {
                if (_cyclicCommission.Entries.FindIndex(p => p.Lecturer.Id == lecturer.Id) != -1)
                    continue;
                _cyclicCommission.Add(lecturer, DataStatus.New);
            }
            _cyclicCommission.Name = textBox1.Text;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (lecComboBox.SelectedItem == null)
                return;

            var lec = (lecComboBox.SelectedItem as LecComboBoxItem).Lecturer;

            if (_lecturers.Find(p => p.Id == lec.Id) != null)
                return;

            _lecturers.Add(lec);
            UpdateView();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;
            var lec = (listView1.SelectedItems[0] as LecturerListItem).Lecturer;

            _lecturers.Remove(lec);
            UpdateView();
        }

        private void UpdateView()
        {
            listView1.Items.Clear();

            foreach (var lecturer in _lecturers)
            {
                listView1.Items.Add(new LecturerListItem(new string[]
                                                             {
                                                                 lecturer.FIO
                                                             })
                                        {
                                            Lecturer = lecturer
                                        });
            }
        }
    }
}
