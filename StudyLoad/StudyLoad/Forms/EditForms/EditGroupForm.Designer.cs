﻿namespace StudyLoad
{
    partial class EditGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yearNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.contractNum = new System.Windows.Forms.NumericUpDown();
            this.budgetNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.sumText = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupNum = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.yearNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.budgetNum)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Номер группы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(12, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Год поступления";
            // 
            // yearNum
            // 
            this.yearNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.yearNum.Location = new System.Drawing.Point(160, 82);
            this.yearNum.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.yearNum.Minimum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.yearNum.Name = "yearNum";
            this.yearNum.Size = new System.Drawing.Size(62, 26);
            this.yearNum.TabIndex = 3;
            this.yearNum.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label3.Location = new System.Drawing.Point(12, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Контракт";
            // 
            // contractNum
            // 
            this.contractNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.contractNum.Location = new System.Drawing.Point(160, 114);
            this.contractNum.Name = "contractNum";
            this.contractNum.Size = new System.Drawing.Size(62, 26);
            this.contractNum.TabIndex = 5;
            this.contractNum.ValueChanged += new System.EventHandler(this.contractNum_ValueChanged);
            // 
            // budgetNum
            // 
            this.budgetNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.budgetNum.Location = new System.Drawing.Point(160, 146);
            this.budgetNum.Name = "budgetNum";
            this.budgetNum.Size = new System.Drawing.Size(62, 26);
            this.budgetNum.TabIndex = 7;
            this.budgetNum.ValueChanged += new System.EventHandler(this.contractNum_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label4.Location = new System.Drawing.Point(12, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Бюджет";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // sumText
            // 
            this.sumText.AutoSize = true;
            this.sumText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.sumText.Location = new System.Drawing.Point(15, 193);
            this.sumText.Name = "sumText";
            this.sumText.Size = new System.Drawing.Size(112, 20);
            this.sumText.TabIndex = 8;
            this.sumText.Text = "Всего людей:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button1.Location = new System.Drawing.Point(12, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 33);
            this.button1.TabIndex = 9;
            this.button1.Text = "Отмена";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.CancelClick);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.button2.Location = new System.Drawing.Point(262, 241);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(83, 33);
            this.button2.TabIndex = 10;
            this.button2.Text = "Принять";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.AcceptButtonClick);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(160, 47);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 28);
            this.comboBox1.Sorted = true;
            this.comboBox1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label5.Location = new System.Drawing.Point(12, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "Отделение";
            // 
            // groupNum
            // 
            this.groupNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupNum.Location = new System.Drawing.Point(160, 14);
            this.groupNum.Mask = "000";
            this.groupNum.Name = "groupNum";
            this.groupNum.Size = new System.Drawing.Size(100, 26);
            this.groupNum.TabIndex = 0;
            // 
            // EditGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(357, 286);
            this.Controls.Add(this.groupNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.sumText);
            this.Controls.Add(this.budgetNum);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.contractNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.yearNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Name = "EditGroupForm";
            this.Text = "Редактирование группы";
            this.TransparencyKey = System.Drawing.Color.DarkRed;
            this.Load += new System.EventHandler(this.EditGroupForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.yearNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contractNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.budgetNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yearNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown contractNum;
        private System.Windows.Forms.NumericUpDown budgetNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label sumText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox groupNum;
    }
}