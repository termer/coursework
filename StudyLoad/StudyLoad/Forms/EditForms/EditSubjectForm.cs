﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;
using StudyLoad.Forms.EditForms;

namespace StudyLoad
{
    public partial class EditSubjectForm : ErrorForm
    {

        private class ComboBoxSubjectTypeItem
        {
            private SubjectTypeVO _subject;
            public SubjectTypeVO Subject
            {
                get { return _subject; }
            }

            public ComboBoxSubjectTypeItem(SubjectTypeVO subject)
            {
                _subject = subject;
            }

            public override string ToString()
            {
                return _subject.Name;
            }
        }

        public SubjectVO Subject;

        private SubjectVO.Semester _semester;
        private SubjectVO.Semester _semester2;

        private SubjectVO.Semester _currentSemester;

        public EditSubjectForm(SubjectVO subject = null)
        {
            InitializeComponent();

            var subjectsTypes = new SubjectTypeBUS().GetSubjectsTypes();
            comboBox1.Items.AddRange(subjectsTypes.Select(p => new ComboBoxSubjectTypeItem(p)).ToArray());

            if(comboBox1.Items.Count > 0)
            comboBox1.SelectedItem = comboBox1.Items[0];

            if (subject == null)
                subject = new SubjectVO(DataStatus.New);
            else
            {
                nameText.Text = subject.Name;
                codeText.Text = subject.Code;

                _semester = subject.Semester1;
                _semester2 = subject.Semester2;

                _currentSemester = _semester;

                foreach (var citem in comboBox1.Items.Cast<object>().Select(item => (item as ComboBoxSubjectTypeItem).Subject).Where(citem => citem.Name == subject.SubjectType.Name))
                {
                    comboBox1.SelectedItem = citem;
                    break;
                }

                

                GuiUpdateValues();
            }

            

            
            

            Subject = subject;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Ok_Click(object sender, EventArgs e)
        {
            bool ok = true;
            if (nameText.Text == "")
            {
                ok = false;
                ShowError(nameText, "Пустое имя");
            }
            else
            {
                RemoveError(nameText);
            }
            if (codeText.Text == "")
            {
                ok = false;
                ShowError(codeText, "Пустой код");
            }
            else
            {
                RemoveError(codeText);
            }
            if (comboBox1.SelectedItem == null)
            {
                ok = false;
                ShowError(comboBox1, "Пустое имя");
            }
            else
            {
                RemoveError(comboBox1);
            }

            SetCurrentSemester();

            if (_semester.OverallTime == 0 && _semester2.OverallTime == 0)
            {
                ShowError(Sum, "пустые семестры");
                ok = false;
            }
            else
            {
                RemoveError(Sum);
            }

            if (!ok)
            {
                return;
            }

            try
            {
                Subject.SubjectType = ((ComboBoxSubjectTypeItem)(comboBox1.SelectedItem)).Subject;
            }
            catch (Exception)
            {
                ok = false;
                MessageBox.Show("UNKNOWN ERROR CALL A DEV AS SOON AS POSSIBLE!!!");
            }


            if (!ok)
            {
                return;
            }


            




            Subject.Name = nameText.Text;
            Subject.Code = codeText.Text;

            Subject.Semester1 = _semester;
            Subject.Semester2 = _semester2;


            
            DialogResult = DialogResult.OK;
            Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void EditSubjectForm_Load(object sender, EventArgs e)
        {

        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            Sum.Text = "Всего часов: " + (+lecNum.Value + labNum.Value
                                          + courseNum.Value + consultNum.Value
                                          + examNum.Value + creditNum.Value).ToString();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

            CopyValuesToSemester();

            if (numericUpDown1.Value == 2)
            {
                _semester = _currentSemester;
                _currentSemester = _semester2;
            }
            else
            {
                _semester2 = _currentSemester;
                _currentSemester = _semester;
            }

            GuiUpdateValues();
            Refresh();
        }

        private void SetCurrentSemester()
        {
            CopyValuesToSemester();

            if (numericUpDown1.Value == 1)
            {
                _semester = _currentSemester;
                _currentSemester = _semester2;
            }
            else
            {
                _semester2 = _currentSemester;
                _currentSemester = _semester;
            }
        }

        private void GuiUpdateValues()
        {
            lecNum.Value = _currentSemester.LecTime;
            labNum.Value = _currentSemester.LabTime;
            courseNum.Value = _currentSemester.CourseWorkTime;
            consultNum.Value = _currentSemester.ConsultationTime;
            examNum.Value = _currentSemester.ExaminationTime;
            creditNum.Value = _currentSemester.CreditTime;
            weekTimeNumeric.Value = _currentSemester.WeekTime;
            overallNumeric.Value = _currentSemester.OverallTime;
        }

        private void CopyValuesToSemester()
        {
            _currentSemester.LecTime = (int) lecNum.Value;
            _currentSemester.LabTime = (int) labNum.Value;
            _currentSemester.CourseWorkTime = (int) courseNum.Value;
            _currentSemester.ConsultationTime = (int) consultNum.Value;
            _currentSemester.ExaminationTime = (int) examNum.Value;
            _currentSemester.CreditTime = (int) creditNum.Value;
            _currentSemester.WeekTime = (int) weekTimeNumeric.Value;
            _currentSemester.OverallTime = (int) overallNumeric.Value;
        }
    }
}
