﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.Forms;
using StudyLoad.VO;

namespace StudyLoad
{
    public partial class LecturerLoadEditForm : Form
    {
        private readonly AppSettings _settings;

        private class ComboBoxSubjectItem
        {
            public ComboBoxSubjectItem()
            {
                Debug.WriteLine("here");
            }

            public SubjectVO SubjectVO;
            public override string ToString()
            {
                return SubjectVO.Name + " " + SubjectVO.Code + " Всего часов: " + SubjectVO.Overall;
            }
        }
        private class ComboBoxGroupItem
        {
            public GroupVO GroupVO;
            public override string ToString()
            {
                return GroupVO.Department.Name + "-"+ GroupVO.Number;
            }
        }

        public LecturerVO Lecturer { get; private set; }
        private List<EditSubjectDaysForm> _pairs; 

        private GroupBUS _groupBUS;
        private GroupVO[] _groups;

        private SubjectBUS _subjectBUS;
        private SubjectVO[] _subjects;

        public LecturerLoadEditForm(LecturerVO lecturer, AppSettings settings)
        {
            _settings = settings;

            _pairs = new List<EditSubjectDaysForm>();
            InitializeComponent();

            Lecturer = lecturer;


            _groupBUS = new GroupBUS(null);
            _groups = _groupBUS.GetGroups(settings.Year);
            groupComboBox.Items.AddRange(_groups.Select(p => new ComboBoxGroupItem() {GroupVO = p}).ToArray());

            _subjectBUS = new SubjectBUS(new StudyloadBUS());
            _subjects = _subjectBUS.GetSubjects(settings.Year);
            subjectsComboBox.Items.AddRange(
                _subjects.Select(p => new ComboBoxSubjectItem {SubjectVO = p}).ToArray());

            UpdateLecturerFields();
            UpdateListViewItems();
        }

        private void LecturerAddForm_Load(object sender, EventArgs e)
        {
            if (Lecturer.Status == DataStatus.New)
                return;



        }

        private void OkButtonClick(object sender, EventArgs e)
        {
            var lecturerBus = new LecturerBUS();



            foreach (var pair in _pairs)
            {
                pair.BindedSubject.SetDays(pair.JoinPairs(pair.BindedSubject.GetDays()));
            }

            var datedPairs = new List<LecturerDatedPairsVO>();
            var studyload = new StudyloadBUS();
            studyload.Lecturer = Lecturer;
            foreach (var item in _pairs)
            {

                datedPairs.Add(studyload.GetDatedPairs(item.Semester, item.BindedSubject, _settings.Year));
            }

            lecturerBus.UpdateInDataBase(Lecturer, _settings.Year);

            var daysBUS = new LecturerDatedPairBUS(studyload);
            foreach (var datedPair in datedPairs)
            {
                daysBUS.UpdateInDataBase(datedPair, _settings.Year);
            }
            

            

            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }



        private void AddSubjectClick(object sender, EventArgs e)
        {
            var groupItem = (ComboBoxGroupItem)groupComboBox.SelectedItem;
            var subject = (ComboBoxSubjectItem)subjectsComboBox.SelectedItem;

            if (groupItem == null)
                return;
            if (subject == null)
                return;

            var bindedSubjectVO = new BindedSubjectVO
                                      {
                                          Group = groupItem.GroupVO,
                                          Subject = subject.SubjectVO,
                                          Status = DataStatus.New,
                                          Year = _settings.Year
                                      };
            Lecturer.BindedSubjects.Add(bindedSubjectVO);
            

            overallLabel.Text = "Всего часов: " + Lecturer.BindedSubjects.Data.Sum(subjectVO => subjectVO.Subject.Overall);

            UpdateListViewItems();
        }

        private class ListViewSubjectItem:ListViewItem
        {
            public BindedSubjectVO BindedSubjectVO;

            public ListViewSubjectItem(string[] strings)
                :base(strings)
            {

            }
        }

        private void UpdateLecturerFields()
        {

            lecturerName.Text = Lecturer.FIO;
            //   nameText.Text = Lecturer.FIO;
            //   rateNumeric.Value = Lecturer.Rate;
        }

        private void UpdateListViewItems()
        {
            subjectsListView.Items.Clear();

            foreach (var bs in Lecturer.BindedSubjects.Data.Where(p => p.Status != DataStatus.Removed))
            {
                var listViewItem =
                    new ListViewSubjectItem(new []
                                                {
                                                    bs.Subject.Name,
                                                    bs.Subject.Code,
                                                    bs.Subject.Semester1.Overall.ToString(),
                                                    bs.Subject.Semester2.Overall.ToString(),
                                                    bs.Subject.Overall.ToString(),
                                                    bs.Group.FullName
                                                })
                        {
                            BindedSubjectVO = bs
                        };


                subjectsListView.Items.Add(listViewItem);
            }
        }

        private void removeSubjectButton_Click(object sender, EventArgs e)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;

            var viewSubjectItem = (ListViewSubjectItem)subjectsListView.SelectedItems[0];
            if (viewSubjectItem == null)
                return;

            var subject = viewSubjectItem.BindedSubjectVO;

            Lecturer.BindedSubjects.Remove(subject);
            
            UpdateListViewItems();
        }

        private void changeDaysButton_Click(object sender, EventArgs e)
        {
            ChangeSemesterDays(1);
        }
        private void changeDays2_Click(object sender, EventArgs e)
        {
            ChangeSemesterDays(2);
        }

        private void ChangeSemesterDays(int semester)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;

            var viewSubjectItem = (ListViewSubjectItem) subjectsListView.SelectedItems[0];
            if (viewSubjectItem == null)
                return;

            var subject = viewSubjectItem.BindedSubjectVO;

            
            var editDaysForm = _pairs.Find(p => p.BindedSubject == subject && p.Semester == semester) ??
                               new EditSubjectDaysForm(_settings, Lecturer, subject, semester);

            var result = editDaysForm.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                if (!_pairs.Contains(editDaysForm))
                    _pairs.Add(editDaysForm);
            }
        }
    }
}
