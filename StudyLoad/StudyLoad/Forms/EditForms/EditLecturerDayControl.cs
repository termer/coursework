﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class EditLecturerDayControl : UserControl
    {
        public readonly LecturerDayVO Day;

        private class PairListViewItem : ListViewItem
        {
            public LecturerDayVO.Pair Pair { get; private set; }

            public PairListViewItem(string[] strings, LecturerDayVO.Pair pair)
                :base(strings)
            {
                Pair = pair;
            }
        }

        public List<LecturerDayVO.Pair> Pairs { get; private set; }

        public EditLecturerDayControl(LecturerDayVO day)
        {
            InitializeComponent();
            Day = day;
            Pairs = day.GetPairs();
            UpdateListView();

            dayLabel.Text = DateHelper.GetDayName(Day.Day, true);
        }

        private void add_Click(object sender, EventArgs e)
        {

            var pair = new LecturerDayVO.Pair
                           {
                               PairState = LecturerDayVO.Pair.LecturerPairState.Usual
                           };

            if((string) comboBox1.SelectedItem == "Первая неделя")
                pair.PairState = LecturerDayVO.Pair.LecturerPairState.WeekOne;
            else if((string) comboBox1.SelectedItem == "Вторая неделя")
                pair.PairState = LecturerDayVO.Pair.LecturerPairState.WeekTwo;

            pair.Hours = (int)numericUpDown1.Value;

            Pairs.Add(pair); 
            UpdateListView();
        }

        private void remove_Click(object sender, EventArgs e)
        {
            if(listView1.SelectedItems.Count == 0)
                return;

            var item = (PairListViewItem)listView1.SelectedItems[0];
            var pair = item.Pair;

            Pairs.Remove(pair);
            UpdateListView();
        }

        private void UpdateListView()
        {
            listView1.Items.Clear();
            foreach (var pair in Pairs)
            {
                listView1.Items.Add(new PairListViewItem(new []
                                                {
                                                    pair.Hours.ToString(),
                                                    pair.PairState.ToString()
                                                }, pair));
            }

        }
    }
}
