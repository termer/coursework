﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.DataHolders;
using StudyLoad.VO;
using StudyLoad.Forms.EditForms;

namespace StudyLoad.Forms
{
    public partial class EditLecturerForm : ErrorForm
    {
        public LecturerVO Lecturer;

        public EditLecturerForm(LecturerVO lecturer = null)
        {
            InitializeComponent();

            if (lecturer == null)
            {
                lecturer = new LecturerVO {Status = DataStatus.New};
            }
            Lecturer = lecturer;
            UpdateLecturerFields();
        }

        private void UpdateLecturerFields()
        {


            nameText.Text = Lecturer.FIO;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (nameText.Text.Length == 0)
            {
                ShowError(nameText, "Пустое имя");
                return;
            }

            Lecturer.FIO = nameText.Text;
            Lecturer.Rate = 0;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
