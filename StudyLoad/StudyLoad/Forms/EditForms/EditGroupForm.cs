﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;
using StudyLoad.Forms.EditForms;

namespace StudyLoad
{
    public partial class EditGroupForm : ErrorForm
    {
        private readonly AppSettings _settings;
        public readonly GroupVO GroupVO;

        private class ComboBoxDepartmentItem
        {
            private DepartmentVO _department;
            public DepartmentVO Department
            {
                get { return _department; }
            }

            public ComboBoxDepartmentItem(DepartmentVO department)
            {
                _department = department;
            }

            public override string ToString()
            {
                return _department.Name;
            }
        }


        public EditGroupForm(AppSettings settings, GroupVO groupVO = null)
        {
            InitializeComponent();

            var departments = new DepartmentBUS().GetDepartments(settings.Year);

            comboBox1.Items.AddRange(departments.Select(p => new ComboBoxDepartmentItem(p)).ToArray());

            if (groupVO == null)
                groupVO = new GroupVO { Status = DataStatus.New };
            else
            {
                groupNum.Text = groupVO.Number.ToString();
                budgetNum.Value = groupVO.BudgetCount;
                contractNum.Value = groupVO.ContractCount;
                yearNum.Value = groupVO.AdmissionYear;
            }

            _settings = settings;
            GroupVO = groupVO;
        }
        
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void AcceptButtonClick(object sender, EventArgs e)
        {
            bool ok = true;
            if (string.IsNullOrEmpty(groupNum.Text))
            {
                ShowError(groupNum, "Пустой номер");
                ok = false;
            }
            else 
            {
                RemoveError(groupNum);
            }

            if (contractNum.Value + budgetNum.Value == 0)
            {
                ShowError(sumText, "Пустая группа");
                ok = false;
            }
            else 
            {
                RemoveError(sumText);
            }

            if (comboBox1.SelectedIndex == -1)
            {
                ShowError(comboBox1, "Не выбранно отделение");
                ok = false;
            }
            else
            {
                RemoveError(comboBox1);
            }

            if (!ok)
            {
                return;
            }

            GroupVO.Number = groupNum.Text;
            GroupVO.AdmissionYear = (int)yearNum.Value;
            GroupVO.ContractCount = (int)contractNum.Value;
            GroupVO.BudgetCount = (int) budgetNum.Value;
            GroupVO.Department = ((ComboBoxDepartmentItem)(comboBox1.SelectedItem)).Department;


            //var groupBus = new GroupBUS();
            //groupBus.UpdateInDataBase(GroupVO);
            DialogResult = DialogResult.OK;
            
            Close();
        }

        private void CancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void EditGroupForm_Load(object sender, EventArgs e)
        {

        }

        private void contractNum_ValueChanged(object sender, EventArgs e)
        {
            sumText.Text =  "Всего: " + (contractNum.Value + budgetNum.Value).ToString();
        }
    }
}
