﻿namespace StudyLoad
{
    partial class LecturerLoadEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.overallLabel = new System.Windows.Forms.Label();
            this.lecturerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.subjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.subjectsListView = new System.Windows.Forms.ListView();
            this.name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.code = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hoursSemester_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hoursSemester_2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hoursSum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.group = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.subjectsComboBox = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.removeSubjectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lecturerName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.changeDaysButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lecturerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(790, 580);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 38);
            this.button1.TabIndex = 4;
            this.button1.Text = "Принять";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OkButtonClick);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(13, 580);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 38);
            this.button2.TabIndex = 5;
            this.button2.Text = "Отмена";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.CancelButtonClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Предметы:";
            // 
            // overallLabel
            // 
            this.overallLabel.AutoSize = true;
            this.overallLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overallLabel.Location = new System.Drawing.Point(18, 529);
            this.overallLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.overallLabel.Name = "overallLabel";
            this.overallLabel.Size = new System.Drawing.Size(105, 20);
            this.overallLabel.TabIndex = 8;
            this.overallLabel.Text = "Всего часов:";
            // 
            // subjectsListView
            // 
            this.subjectsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.code,
            this.hoursSemester_1,
            this.hoursSemester_2,
            this.hoursSum,
            this.group});
            this.subjectsListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsListView.FullRowSelect = true;
            this.subjectsListView.LabelWrap = false;
            this.subjectsListView.Location = new System.Drawing.Point(18, 138);
            this.subjectsListView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.subjectsListView.MultiSelect = false;
            this.subjectsListView.Name = "subjectsListView";
            this.subjectsListView.Size = new System.Drawing.Size(924, 244);
            this.subjectsListView.TabIndex = 9;
            this.subjectsListView.UseCompatibleStateImageBehavior = false;
            this.subjectsListView.View = System.Windows.Forms.View.Details;
            // 
            // name
            // 
            this.name.Text = "Название";
            this.name.Width = 111;
            // 
            // code
            // 
            this.code.Text = "Код";
            this.code.Width = 71;
            // 
            // hoursSemester_1
            // 
            this.hoursSemester_1.Text = "Часы 1 семестр";
            this.hoursSemester_1.Width = 133;
            // 
            // hoursSemester_2
            // 
            this.hoursSemester_2.Text = "Часы 2 семестр";
            this.hoursSemester_2.Width = 143;
            // 
            // hoursSum
            // 
            this.hoursSum.Text = "Всего часов";
            this.hoursSum.Width = 161;
            // 
            // group
            // 
            this.group.Text = "Группа";
            this.group.Width = 115;
            // 
            // subjectsComboBox
            // 
            this.subjectsComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.subjectsComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.subjectsComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subjectsComboBox.FormattingEnabled = true;
            this.subjectsComboBox.Location = new System.Drawing.Point(18, 420);
            this.subjectsComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.subjectsComboBox.Name = "subjectsComboBox";
            this.subjectsComboBox.Size = new System.Drawing.Size(786, 28);
            this.subjectsComboBox.TabIndex = 10;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(729, 458);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(213, 35);
            this.button3.TabIndex = 11;
            this.button3.Text = "Добавить предмет";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.AddSubjectClick);
            // 
            // groupComboBox
            // 
            this.groupComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.groupComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.HistoryList;
            this.groupComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(812, 420);
            this.groupComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(130, 28);
            this.groupComboBox.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(809, 395);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "Группа";
            // 
            // removeSubjectButton
            // 
            this.removeSubjectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeSubjectButton.Location = new System.Drawing.Point(520, 458);
            this.removeSubjectButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.removeSubjectButton.Name = "removeSubjectButton";
            this.removeSubjectButton.Size = new System.Drawing.Size(201, 35);
            this.removeSubjectButton.TabIndex = 14;
            this.removeSubjectButton.Text = "Удалить предмет";
            this.removeSubjectButton.UseVisualStyleBackColor = true;
            this.removeSubjectButton.Click += new System.EventHandler(this.removeSubjectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 15;
            this.label1.Text = "Преподователь:";
            // 
            // lecturerName
            // 
            this.lecturerName.AutoSize = true;
            this.lecturerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lecturerName.Location = new System.Drawing.Point(168, 20);
            this.lecturerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lecturerName.Name = "lecturerName";
            this.lecturerName.Size = new System.Drawing.Size(51, 20);
            this.lecturerName.TabIndex = 16;
            this.lecturerName.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 395);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Предмет";
            // 
            // changeDaysButton
            // 
            this.changeDaysButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeDaysButton.Location = new System.Drawing.Point(18, 458);
            this.changeDaysButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.changeDaysButton.Name = "changeDaysButton";
            this.changeDaysButton.Size = new System.Drawing.Size(243, 35);
            this.changeDaysButton.TabIndex = 18;
            this.changeDaysButton.Text = "Изменить 1 семестр";
            this.changeDaysButton.UseVisualStyleBackColor = true;
            this.changeDaysButton.Click += new System.EventHandler(this.changeDaysButton_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(269, 458);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(243, 35);
            this.button4.TabIndex = 19;
            this.button4.Text = "Изменить 2 семестр";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.changeDays2_Click);
            // 
            // LecturerLoadEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 627);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.changeDaysButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lecturerName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.removeSubjectButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupComboBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.subjectsComboBox);
            this.Controls.Add(this.subjectsListView);
            this.Controls.Add(this.overallLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "LecturerLoadEditForm";
            this.Text = "Изменение нагрузки преподователя на год";
            this.Load += new System.EventHandler(this.LecturerAddForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lecturerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subjectBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label overallLabel;
        private System.Windows.Forms.BindingSource subjectBindingSource;
        private System.Windows.Forms.BindingSource lecturerBindingSource;
        private System.Windows.Forms.ListView subjectsListView;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader code;
        private System.Windows.Forms.ColumnHeader hoursSemester_1;
        public System.Windows.Forms.ColumnHeader hoursSemester_2;
        private System.Windows.Forms.ColumnHeader hoursSum;
        private System.Windows.Forms.ComboBox subjectsComboBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ColumnHeader group;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button removeSubjectButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lecturerName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button changeDaysButton;
        private System.Windows.Forms.Button button4;
    }
}