﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace StudyLoad.Forms.EditForms
{
    public class ErrorForm:Form
    {

        private Dictionary<Control, Label> _errors = new Dictionary<Control,Label>();
        
        public void ShowError(Control control, string error)
        {
            if (_errors.Keys.Contains(control))
            {
                return;
            }

            Label label = new Label();

            label.Location = new Point(control.Location.X + control.Size.Width, control.Location.Y);
            label.Text = error;
            label.BackColor = Color.Red;    

            _errors[control] = label;
            Controls.Add(label);
        }

        public void RemoveError(Control control)
        {
            if (!_errors.Keys.Contains(control))
            {
                return;
            }

            var textBox = _errors[control];

            Controls.Remove(textBox);
            _errors.Remove(control);
        }



    }
}
