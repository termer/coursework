﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class EditSubjectDaysForm : Form
    {
        public LecturerDatedPairsVO DatedPairsVO { get; private set; }

        private readonly AppSettings _settings;
        private readonly LecturerVO _lecturer;

        public readonly BindedSubjectVO BindedSubject;
        public List<LecturerDayVO> Days;

        public int Semester { get; private set; }


        private List<EditLecturerDayControl> DayElements;

        public EditSubjectDaysForm(AppSettings settings, LecturerVO lecturer, BindedSubjectVO bindedSubject, int semester, LecturerDatedPairsVO datedPairsVO = null)
        {
            _settings = settings;
            _lecturer = lecturer;
            BindedSubject = bindedSubject;
            Semester = semester;
            DatedPairsVO = datedPairsVO;


            InitializeComponent();
            GenerrateComponents();
        }

        public List<LecturerDayVO> JoinPairs(List<LecturerDayVO> lectDays)
        {
            foreach (var dayElement in DayElements)
            {
                var day = lectDays.Find(p => p.Day == dayElement.Day.Day && p.Semester == Semester);

                foreach (var pair in dayElement.Pairs)
                {
                    if (day == null)
                    {
                        day = new LecturerDayVO()
                                  {
                                      Day = dayElement.Day.Day,
                                      Semester = Semester
                                  };
                        lectDays.Add(day);
                    }

                    if(day.GetPairs().FindIndex(p => p.Hours == pair.Hours && p.PairState == pair.PairState) != -1)
                    {
                        continue; 
                    }
                    day.AddPair(pair);
                }

                if(day == null)
                    continue;
                foreach (var pair in day.GetPairs())
                {
                    if (dayElement.Pairs.FindIndex(p => p.Hours == pair.Hours && p.PairState == pair.PairState) != -1)
                    {
                        continue;
                    }
                    day.RemovePair(pair);
                }
            }

            return lectDays;
        }

        private void GenerrateComponents()
        {
            int elementOffset = 16;
            if(Days == null)
            Days = BindedSubject.GetDays().Where(p => p.Semester == Semester).ToList();
            
            DayElements = new List<EditLecturerDayControl>();

            AddDay(Days, DayOfWeek.Monday, ref elementOffset);
            AddDay(Days, DayOfWeek.Tuesday, ref elementOffset);
            AddDay(Days, DayOfWeek.Wednsday, ref elementOffset);
            AddDay(Days, DayOfWeek.Thursday, ref elementOffset);
            AddDay(Days, DayOfWeek.Friday, ref elementOffset);
            AddDay(Days, DayOfWeek.Saturday, ref elementOffset);
            AddDay(Days, DayOfWeek.Sunday, ref elementOffset);
        }

        private void AddDay(List<LecturerDayVO> dayVOs, DayOfWeek day, ref int elementOffset)
        {
            var dayVO = dayVOs.Find(p => p.Day == day) ?? new LecturerDayVO{Day = day, Semester = Semester};
            var dayElement = new EditLecturerDayControl(dayVO)
                                 {
                                     Location = new Point(16, elementOffset)
                                 };

            elementOffset += dayElement.Height;

            DayElements.Add(dayElement);
            Controls.Add(dayElement);

            okButton.Location = new Point(16, elementOffset + okButton.Height);
            cancelButton.Location = new Point(Width - cancelButton.Width - 16,
                                              elementOffset + okButton.Height);
        }

        //public void CopyDays()
        //{
        //    foreach (var element in DayElements)
        //    {
        //        if (Days.Contains(element.Day))
        //        {
        //            foreach (var pair in element.Pairs)
        //            {
        //                element.Day.AddPair(pair);
        //            }
        //        }
        //        else
        //        {
        //            var newDay = new LecturerDayVO { Day = element.Day.Day, Semester = Semester, Status = DataStatus.New};

        //            foreach (var pair in element.Pairs)
        //                newDay.AddPair(pair);

        //            Days.Add(newDay);
        //        }
        //    }
        //}

        private void okButton_Click(object sender, EventArgs e)
        {


            DialogResult = DialogResult.OK;
            Close();
        }

        

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
