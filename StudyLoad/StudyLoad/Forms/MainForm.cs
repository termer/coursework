﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.Controls;
using StudyLoad.DataHolders;
using StudyLoad.Forms;
using StudyLoad.VO;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;

namespace StudyLoad
{
    public partial class MainForm : Form
    {
        private AppSettings _settings;
        private ClosableTab _tabControl;

        public MainForm()
        {
            InitializeComponent();
            CreateTabControl();

            _settings = new AppSettings();

        }

        private void CreateTabControl()
        {
            var location = new Point(0, 32);

            _tabControl = new ClosableTab
                             {
                                 Name = "tabControl1",
                                 SelectedIndex = 0,
                                 Location = location,
                                 Size = new Size(Size.Width - 14 - location.X, Size.Height - 32 - location.Y*2),
                                 TabIndex = 0,
                                 Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom,
                                 Image = new Bitmap("CloseButton.png"),
                                 Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0)
                             };
            _tabControl.Selected += TabControlOnSelected;
            Controls.Add(_tabControl);
        }

        private void TabControlOnSelected(object sender, TabControlEventArgs e)
        {
            if(e.TabPage == null)
                return;
            foreach (var control in e.TabPage.Controls.OfType<IUpdatableListView>())
                control.UpdateData();
        }



        private void CreateTab(string text, params Control[] controls)
        {
            if (TabExist(text))
                return;


            var tabPage = new TabPage(text) {Font = Font};

            tabPage.Controls.AddRange(controls);
            _tabControl.TabPages.Add(tabPage);
            _tabControl.SelectedTab = tabPage;
        }

        private void lecturersToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var form = new ShowLecturersForm(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Преподаватели", form);
        }

        private void groupsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var form = new ShowgroupsForm(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Группы", form);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowloadForm(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Нагрузка", form);
        }

        private bool TabExist(string text)
        {
            return _tabControl.TabPages.ContainsKey(text);
        }

        private void lecturerLoadMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new LecturersReportForm(_settings);

            form.ShowDialog();
        }

        private void subjectTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowSubjectsTypes(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Типы предметов", form);
        }

        private void subjectsForYearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowSubjectsForm(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Предметы", form);
        }

        private void departmentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowDepartments(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Отделения", form);
        }

        private void currentYearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new CurrentYearForm(_settings.Year);

            var result = form.ShowDialog(this);
            if (result == DialogResult.OK)
                _settings.Year = form.CurrentYear;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _settings.Load("settings.xml");


            try
            {
                MySqlHelper.SetConnection("localhost", "root", "4510");
            }
            catch (Exception ex)
            {
                MessageBox.Show("PLEASE CHECK IF MYSQL is installed, and database is created\n" + ex.ToString());
                Close();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _settings.Save("settings.xml");
        }

        private void loadOnMonthToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var month = new SelectMonth();
            var result = month.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            
            var lecturer = new LecturerSelectDialog(_settings);
            result = lecturer.ShowDialog(this);

            if (result != DialogResult.OK)
                return;

            if (lecturer.SelectedLecturer == null)
                return;

            var form = new MonthLoadReport(_settings, lecturer.SelectedLecturer, month.Month);
            form.ShowDialog(this);
        }

        private void cyclicCommissionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowCyclicCommissions(_settings);
            form.Dock = DockStyle.Fill;

            CreateTab("Цикловые комиссии", form);
        }

        private void цикловыеКомиссииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new CyclilcCommissionsReportForm(_settings);
            form.Show();
        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dir = Directory.GetCurrentDirectory() + "\\Help.docx";
           System.Diagnostics.Process.Start(dir);
        }

    }
}
