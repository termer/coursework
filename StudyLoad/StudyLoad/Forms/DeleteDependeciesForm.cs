﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad
{
    public partial class DeleteDependeciesForm : Form
    {
        public DeleteDependeciesForm(LecturerVO[] lecturers)
        {
            InitializeComponent();

            foreach (var lecturerVO in lecturers)
            {
                lecturersListView.Items.Add(new ListViewItem(new[]
                                                                 {
                                                                     lecturerVO.FIO,
                                                                     lecturerVO.Rate.ToString(),
                                                                     lecturerVO.LoadSum.ToString()
                                                                 }));
            }
        }

        private void AcceptClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelClick(object sender, EventArgs e)
        {

            DialogResult = DialogResult.Cancel;
            Close();
        }


    }
}
