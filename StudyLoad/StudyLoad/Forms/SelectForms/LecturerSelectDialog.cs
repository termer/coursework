﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad
{
    public partial class LecturerSelectDialog : Form
    {
        public LecturerVO SelectedLecturer { get; private set; }

        private class ComboBoxLecturerItem
        {
            public LecturerVO Lecturer;
            public override string ToString()
            {
                return Lecturer.FIO;
            }
        }

        public LecturerSelectDialog(AppSettings settings)
        {
            InitializeComponent();

            LecturerBUS lecturerBus = new LecturerBUS();
            var lecturers = lecturerBus.GetLecturersWithLoad(settings.Year);

            foreach (var lecturerVO in lecturers)
            {
                comboBox1.Items.Add(new ComboBoxLecturerItem() {Lecturer = lecturerVO});
            }

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Не выбран преподаватель");
                return;
            }

            SelectedLecturer = ((ComboBoxLecturerItem)(comboBox1.SelectedItem)).Lecturer;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
