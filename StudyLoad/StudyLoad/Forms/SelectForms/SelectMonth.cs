﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudyLoad.Forms
{
    public partial class SelectMonth : Form
    {
        public Month Month { get; private set; }

        private class MonthItem
        {
            public Month month;

            public override string ToString()
            {
                return DateHelper.GetMonthName(month, true);
            }
        }

        public SelectMonth()
        {
            InitializeComponent();

            comboBox1.Items.Add(new MonthItem {month = Month.January});
            comboBox1.Items.Add(new MonthItem {month = Month.February});
            comboBox1.Items.Add(new MonthItem {month = Month.March});
            comboBox1.Items.Add(new MonthItem {month = Month.April});
            comboBox1.Items.Add(new MonthItem {month = Month.May});
            comboBox1.Items.Add(new MonthItem {month = Month.June});
            comboBox1.Items.Add(new MonthItem {month = Month.July});
            comboBox1.Items.Add(new MonthItem {month = Month.August});
            comboBox1.Items.Add(new MonthItem {month = Month.September});
            comboBox1.Items.Add(new MonthItem {month = Month.October});
            comboBox1.Items.Add(new MonthItem {month = Month.November});
            comboBox1.Items.Add(new MonthItem {month = Month.December});
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem == null)
                return;
            Month = ((MonthItem)(comboBox1.SelectedItem)).month;
            
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
