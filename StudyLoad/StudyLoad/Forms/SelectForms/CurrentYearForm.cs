﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudyLoad.Forms
{
    public partial class CurrentYearForm : Form
    {
        public int CurrentYear
        {
            get { return (int)numericUpDown1.Value; }
        }

        public CurrentYearForm(int currentYear)
        {
            InitializeComponent();

            numericUpDown1.Value = currentYear;
        }

        private void okClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
