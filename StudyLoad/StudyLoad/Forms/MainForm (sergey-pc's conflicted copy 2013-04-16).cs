﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.Controls;
using StudyLoad.DataHolders;
using StudyLoad.Forms;
using StudyLoad.VO;
using WeifenLuo.WinFormsUI.Docking;

namespace StudyLoad
{
    public partial class MainForm : Form
    {
        private DockPanel _dockPanel;
        private List<DockContent> _docks;

        private ClosableTab tabControl;

        public MainForm()
        {
            InitializeComponent();
            CreateTabControl();
        }

        private void CreateTabControl()
        {
            var location = new Point(0, 32);

            tabControl = new ClosableTab
                             {
                                 Name = "tabControl1",
                                 SelectedIndex = 0,
                                 Location = location,
                                 Size = new Size(Size.Width - 14 - location.X, Size.Height - 32 - location.Y*2),
                                 TabIndex = 0,
                                 Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom,
                                 Image = new Bitmap("CloseButton.png"),
                                 Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0)
                             };
            tabControl.Selected += TabControlOnSelected;
            Controls.Add(tabControl);
        }

        private void TabControlOnSelected(object sender, TabControlEventArgs e)
        {
            if(e.TabPage == null)
                return;
            foreach (var control in e.TabPage.Controls.OfType<IUpdatableListView>())
                control.UpdateData();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void CreateTab(string text, params Control[] controls)
        {
            if (TabExist(text))
                return;


            var tabPage = new TabPage(text) {Font = Font};

            tabPage.Controls.AddRange(controls);
            tabControl.TabPages.Add(tabPage);
            tabControl.SelectedTab = tabPage;
        }

        private void lecturersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowLecturersForm();
            form.Dock = DockStyle.Fill;

            CreateTab("Преподователи", form);
        }

        private void groupsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var form = new ShowgroupsForm();
            form.Dock = DockStyle.Fill;

            CreateTab("Группы", form);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowloadForm();
            form.Dock = DockStyle.Fill;

            CreateTab("Нагрузка", form);
        }

        private bool TabExist(string text)
        {
            return tabControl.TabPages.ContainsKey(text);
        }

        private void lecturerLoadMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new LecturersReportForm(2000);

            form.ShowDialog();
        }

        private void subjectTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowSubjectsTypes();
            form.Dock = DockStyle.Fill;

            CreateTab("Типы предметов", form);
        }

        private void subjectsForYearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowSubjectsForm();
            form.Dock = DockStyle.Fill;

            CreateTab("Предметы", form);
        }

        private void departmentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ShowDepartments();
            form.Dock = DockStyle.Fill;

            CreateTab("Отделения", form);
        }

    }
}
