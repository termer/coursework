﻿namespace StudyLoad
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lecturersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectsForYearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groups1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentYearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cyclicCommissionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нагрузкаНаПреподователйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadOnMonthToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.цикловыеКомиссииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(855, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lecturersToolStripMenuItem,
            this.subjectsToolStripMenuItem,
            this.groups1ToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.currentYearToolStripMenuItem,
            this.cyclicCommissionsToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(78, 25);
            this.dataToolStripMenuItem.Text = "Данные";
            // 
            // lecturersToolStripMenuItem
            // 
            this.lecturersToolStripMenuItem.Name = "lecturersToolStripMenuItem";
            this.lecturersToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.lecturersToolStripMenuItem.Text = "Преподаватели";
            this.lecturersToolStripMenuItem.Click += new System.EventHandler(this.lecturersToolStripMenuItem_Click);
            // 
            // subjectsToolStripMenuItem
            // 
            this.subjectsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subjectsForYearToolStripMenuItem,
            this.subjectTypesToolStripMenuItem});
            this.subjectsToolStripMenuItem.Name = "subjectsToolStripMenuItem";
            this.subjectsToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.subjectsToolStripMenuItem.Text = "Предметы";
            // 
            // subjectsForYearToolStripMenuItem
            // 
            this.subjectsForYearToolStripMenuItem.Name = "subjectsForYearToolStripMenuItem";
            this.subjectsForYearToolStripMenuItem.Size = new System.Drawing.Size(203, 26);
            this.subjectsForYearToolStripMenuItem.Text = "Предметы на год";
            this.subjectsForYearToolStripMenuItem.Click += new System.EventHandler(this.subjectsForYearToolStripMenuItem_Click);
            // 
            // subjectTypesToolStripMenuItem
            // 
            this.subjectTypesToolStripMenuItem.Name = "subjectTypesToolStripMenuItem";
            this.subjectTypesToolStripMenuItem.Size = new System.Drawing.Size(203, 26);
            this.subjectTypesToolStripMenuItem.Text = "Типы предметов";
            this.subjectTypesToolStripMenuItem.Click += new System.EventHandler(this.subjectTypesToolStripMenuItem_Click);
            // 
            // groups1ToolStripMenuItem
            // 
            this.groups1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.groupsToolStripMenuItem,
            this.departmentsToolStripMenuItem});
            this.groups1ToolStripMenuItem.Name = "groups1ToolStripMenuItem";
            this.groups1ToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.groups1ToolStripMenuItem.Text = "Группы";
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.groupsToolStripMenuItem.Text = "Группы";
            this.groupsToolStripMenuItem.Click += new System.EventHandler(this.groupsToolStripMenuItem_Click);
            // 
            // departmentsToolStripMenuItem
            // 
            this.departmentsToolStripMenuItem.Name = "departmentsToolStripMenuItem";
            this.departmentsToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.departmentsToolStripMenuItem.Text = "Отделения";
            this.departmentsToolStripMenuItem.Click += new System.EventHandler(this.departmentsToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.loadToolStripMenuItem.Text = "Нагрузка";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // currentYearToolStripMenuItem
            // 
            this.currentYearToolStripMenuItem.Name = "currentYearToolStripMenuItem";
            this.currentYearToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.currentYearToolStripMenuItem.Text = "Текущий год";
            this.currentYearToolStripMenuItem.Click += new System.EventHandler(this.currentYearToolStripMenuItem_Click);
            // 
            // cyclicCommissionsToolStripMenuItem
            // 
            this.cyclicCommissionsToolStripMenuItem.Name = "cyclicCommissionsToolStripMenuItem";
            this.cyclicCommissionsToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cyclicCommissionsToolStripMenuItem.Size = new System.Drawing.Size(226, 26);
            this.cyclicCommissionsToolStripMenuItem.Text = "Цикловые комиссии";
            this.cyclicCommissionsToolStripMenuItem.Click += new System.EventHandler(this.cyclicCommissionsToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нагрузкаНаПреподователйToolStripMenuItem,
            this.loadOnMonthToolStripMenuItem,
            this.цикловыеКомиссииToolStripMenuItem});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(76, 25);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // нагрузкаНаПреподователйToolStripMenuItem
            // 
            this.нагрузкаНаПреподователйToolStripMenuItem.Name = "нагрузкаНаПреподователйToolStripMenuItem";
            this.нагрузкаНаПреподователйToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.нагрузкаНаПреподователйToolStripMenuItem.Text = "Нагрузка преподавателей";
            this.нагрузкаНаПреподователйToolStripMenuItem.Click += new System.EventHandler(this.lecturerLoadMenuToolStripMenuItem_Click);
            // 
            // loadOnMonthToolStripMenuItem
            // 
            this.loadOnMonthToolStripMenuItem.Name = "loadOnMonthToolStripMenuItem";
            this.loadOnMonthToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.loadOnMonthToolStripMenuItem.Text = "Нагрузка на месяц";
            this.loadOnMonthToolStripMenuItem.Click += new System.EventHandler(this.loadOnMonthToolStripMenuItem_Click);
            // 
            // цикловыеКомиссииToolStripMenuItem
            // 
            this.цикловыеКомиссииToolStripMenuItem.Name = "цикловыеКомиссииToolStripMenuItem";
            this.цикловыеКомиссииToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.цикловыеКомиссииToolStripMenuItem.Text = "Цикловые комиссии";
            this.цикловыеКомиссииToolStripMenuItem.Click += new System.EventHandler(this.цикловыеКомиссииToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(82, 25);
            this.справкаToolStripMenuItem.Text = "Справка";
            this.справкаToolStripMenuItem.Click += new System.EventHandler(this.справкаToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(855, 435);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Нагрузка";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lecturersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groups1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нагрузкаНаПреподователйToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectsForYearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subjectTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem groupsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem currentYearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadOnMonthToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cyclicCommissionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem цикловыеКомиссииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
    }
}