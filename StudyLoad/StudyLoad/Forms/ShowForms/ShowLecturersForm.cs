﻿using System;
using System.Linq;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowLecturersForm : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        public DataManager<LecturerVO> Lecturers;

        private LecturerBUS _lecturerBUS;


        public ShowLecturersForm(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();

            Lecturers = new DataManager<LecturerVO>();
            _lecturerBUS = new LecturerBUS();

            UpdateData();
        }

        public void UpdateData()
        {


            var lecturers = _lecturerBUS.GetLecturersWithLoad(_settings.Year);
            Lecturers.Data.Clear();
            Lecturers.Add(lecturers);

            UpdateView();
            Refresh();
        }

        private void UpdateView()
        {
            lecturersListView.Items.Clear();
            foreach (var lecturerVO in Lecturers.Data.Where(p => p.FIO.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                lecturersListView.Items.Add(new LecturerListItem(new[]
                                                                     {
                                                                         lecturerVO.FIO,
                                                                         lecturerVO.Rate.ToString(),
                                                                     })
                                                {
                                                    LecturerVO = lecturerVO
                                                });
            }
        }

        private void ShowLecturersForm_Load(object sender, EventArgs e)
        {
            
        }


        private class LecturerListItem : ListViewItem
        {
            public LecturerVO LecturerVO;

            public LecturerListItem(string[] strings)
                :base(strings)
            {
            }
        }

 


        private void lecturersListView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddButton_Click_1(object sender, EventArgs e)
        {
            var editLecturerForm = new EditLecturerForm();
            var result = editLecturerForm.ShowDialog();

            if(result != DialogResult.OK)
                return;
            Lecturers.Add(editLecturerForm.Lecturer);
            Lecturers.Update(_lecturerBUS, _settings.Year);

            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (lecturersListView.SelectedItems.Count == 0)
                return;
            var lecturer = ((LecturerListItem)lecturersListView.SelectedItems[0]).LecturerVO;

            Lecturers.Remove(lecturer);
            Lecturers.Update(_lecturerBUS, _settings.Year);

            UpdateData();
        }

        private void editButton_Click_1(object sender, EventArgs e)
        {
            if (lecturersListView.SelectedItems.Count == 0)
                return;

            var editLecturerForm = new EditLecturerForm(((LecturerListItem)lecturersListView.SelectedItems[0]).LecturerVO);
            editLecturerForm.ShowDialog(this);

            Lecturers.Update(_lecturerBUS, _settings.Year);
            UpdateData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            UpdateView();
        }
    }
}
