﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowCyclicCommissions : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        private DataManager<CyclicCommissionVO> _comissions;
        private CyclicCommissionBUS _comissionsBUS;

        public ShowCyclicCommissions(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
            _comissions = new DataManager<CyclicCommissionVO>();
            _comissionsBUS = new CyclicCommissionBUS();

            UpdateData();
        }

        private class CommissionListItem : ListViewItem
        {
            public CyclicCommissionVO Commission;
            public CommissionListItem(string[] strings)
                : base(strings)
            {

            }
        }

        public void UpdateView()
        {
            departmentsListView.Items.Clear();
            foreach (var c in _comissions.Data.Where(p => p.Name.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                departmentsListView.Items.Add(new CommissionListItem(new[]
                                                                   {
                                                                       c.Name,
                                                                   })
                {
                    Commission = c
                });
            }
        }

        public void UpdateData()
        {
            _comissions.Data.Clear();
            _comissions.Add(_comissionsBUS.GetCyclicCommissions(_settings.Year));
            UpdateView();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var subjectsForm = new EditCyclicCommissionForm(null, _settings);
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;

            _comissionsBUS.UpdateInDataBase(subjectsForm.CyclicCommission, _settings.Year);
            UpdateData();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (departmentsListView.SelectedItems.Count == 0)
                return;
            var commission = (departmentsListView.SelectedItems[0] as CommissionListItem).Commission;

            var subjectsForm = new EditCyclicCommissionForm(commission, _settings);
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;

            _comissionsBUS.UpdateInDataBase(subjectsForm.CyclicCommission, _settings.Year);
            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (departmentsListView.SelectedItems.Count == 0)
                return;

            var subject = (departmentsListView.SelectedItems[0] as CommissionListItem).Commission;


            UpdateData();
        }

        private void nameFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }
    }
}
