﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowSubjectsTypes : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        private DataManager<SubjectTypeVO> _subjectsTypes;
        private SubjectTypeBUS subjectBUS;
        public ShowSubjectsTypes(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
            _subjectsTypes = new DataManager<SubjectTypeVO>();
            subjectBUS = new SubjectTypeBUS();

            UpdateData();
        }

        private class SubjectTypeListItem : ListViewItem
        {
            public SubjectTypeVO SubjectTypeVO;
            public SubjectTypeListItem(string[] strings)
                : base(strings)
            {

            }
        }

        public void UpdateView()
        {
            subjectsListView.Items.Clear();
            foreach (var subject in _subjectsTypes.Data.Where(p => p.Name.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                subjectsListView.Items.Add(new SubjectTypeListItem(new[]
                                                                   {
                                                                       subject.Name,
                                                                   })
                {
                    SubjectTypeVO = subject
                });
            }
        }

        public void UpdateData()
        {
            _subjectsTypes.Data.Clear();
            _subjectsTypes.Add(subjectBUS.GetSubjectsTypes());
            UpdateView();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var subjectsForm = new EditSubjectType();
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            _subjectsTypes.Add(subjectsForm.Subject);
            _subjectsTypes.Update(subjectBUS, 0);
            UpdateData();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;
            var subject = (subjectsListView.SelectedItems[0] as SubjectTypeListItem).SubjectTypeVO;

            var subjectsForm = new EditSubjectType(subject);
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            _subjectsTypes.Update(subjectBUS, 0);
            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;

            var subject = (subjectsListView.SelectedItems[0] as SubjectTypeListItem).SubjectTypeVO;

            _subjectsTypes.Remove(subject);
            _subjectsTypes.Update(subjectBUS, _settings.Year);

            UpdateData();
        }

        private void nameFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
            
        }
    }
}
