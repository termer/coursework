﻿using System;
using System.Linq;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowgroupsForm : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        public DataManager<GroupVO> Groups;
        
        private GroupBUS _groupBUS;

        public ShowgroupsForm(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();

            Groups = new DataManager<GroupVO>();

            _groupBUS = new GroupBUS(new StudyloadBUS());

            UpdateData();
        }

        public void UpdateData()
        {


            var groups = _groupBUS.GetGroups(_settings.Year);
            Groups.Data.Clear();
            foreach (var lecturerVO in groups)
            {
                Groups.Add(lecturerVO);
            }

            UpdateView();
            Refresh();
        }

        private void UpdateView()
        {
            groupsListView.Items.Clear();
            foreach (var groupsVO in Groups.Data.Where(p => p.FullName.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                groupsListView.Items.Add(new GroupListItem(new[]
                                                               {
                                                                   groupsVO.FullName,
                                                                   groupsVO.AdmissionYear.ToString(),
                                                                   groupsVO.BudgetCount.ToString(),
                                                                   groupsVO.ContractCount.ToString(),
                                                                   groupsVO.StudentsCount.ToString()
                                                               })
                                             {
                                                 GroupVO = groupsVO
                                             });
            }
        }

        private class GroupListItem : ListViewItem
        {
            public GroupVO GroupVO;

            public GroupListItem(string[] strings)
                : base(strings)
            {
            }
        }



        private void AddButton_Click_1(object sender, EventArgs e)
        {
            var editGroupForm = new EditGroupForm(_settings);
            var result = editGroupForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;

            Groups.Add(editGroupForm.GroupVO);
            Groups.Update(_groupBUS, _settings.Year);

            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (groupsListView.SelectedItems.Count == 0)
                return;
            var group = ((GroupListItem)(groupsListView.SelectedItems[0])).GroupVO;


            var dependableLecturers = _groupBUS.GetDependableLecturers(group.Id, new LecturerBUS());
            if (dependableLecturers.Count() != 0)
            {
                var deleteDepenForm = new DeleteDependeciesForm(dependableLecturers);
                var result = deleteDepenForm.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;
            }



            Groups.Remove(group);
            Groups.Update(_groupBUS, _settings.Year);
            UpdateData();
        }

        private void editButton_Click_1(object sender, EventArgs e)
        {
            if (groupsListView.SelectedItems.Count == 0)
                return;
            var group = ((GroupListItem)(groupsListView.SelectedItems[0])).GroupVO;

            var editGroupForm = new EditGroupForm(_settings, group);
            editGroupForm.ShowDialog(this);
            Groups.Update(_groupBUS, _settings.Year);
            UpdateData();
        }

        private void ShowgroupsForm_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }




    }
}
