﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;
using System.IO;

namespace StudyLoad.Forms
{
    public partial class ShowloadForm : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        public DataManager<LecturerVO> Lecturers;

        private LecturerBUS _lecturerBUS;

        public ShowloadForm(AppSettings settings)
        {
            _settings = settings;
            _lecturerBUS = new LecturerBUS();
            Lecturers = new DataManager<LecturerVO>();

            InitializeComponent();
            UpdateData();

            settings.YearChanged += SettingsOnYearChanged;
        }
        private void SettingsOnYearChanged(object sender, EventArgs eventArgs)
        {
            UpdateData();
        }



        private void AddButton_Click(object sender, EventArgs e)
        {
          //  try
            {
                var selectLecturerForm = new LecturerSelectDialog(_settings);
                var result = selectLecturerForm.ShowDialog();
                if (result == DialogResult.Cancel)
                    return;

                var editLoadForm = new LecturerLoadEditForm(selectLecturerForm.SelectedLecturer, _settings);
                editLoadForm.ShowDialog();

                //_lecturerBUS.UpdateInDataBase(selectLecturerForm.SelectedLecturer);
                Lecturers.Update(_lecturerBUS, _settings.Year);
                UpdateData();   
            }
     //       catch
            {

            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
          //  try
            {
                if (lecturersListView.SelectedItems.Count == 0)
                    return;

                var selectedLecturer = (lecturersListView.SelectedItems[0] as LecturerListItem).LecturerVO;

                var editLoadForm = new LecturerLoadEditForm(selectedLecturer, _settings);
                editLoadForm.ShowDialog();

                Lecturers.Update(_lecturerBUS, _settings.Year);

                UpdateData();
            }
        //    catch (Exception)
            {
            }
        }


        private class LecturerListItem : ListViewItem
        {
            public LecturerVO LecturerVO;
            public LecturerListItem(string[] strings)
                : base(strings)
            {
            }
        }

        public void UpdateData()
        {
         //   try
            {
                var lecturers = _lecturerBUS.GetLecturersWithLoad(_settings.Year);

                Lecturers.Data.Clear();
                Lecturers.Add(lecturers.Where(p => p.LoadSum != 0).ToArray());
                UpdateView();
                Refresh();
            }
          //  catch (Exception)
            {

            }
        }

        private void UpdateView()
        {
            lecturersListView.Items.Clear();
            foreach (var lecturerVO in Lecturers.Data.Where(p => p.LoadSum != 0 && 
                                                                 p.FIO.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                lecturersListView.Items.Add(new LecturerListItem(new[]
                                                                     {
                                                                         lecturerVO.FIO,
                                                                         lecturerVO.Rate.ToString(),
                                                                         lecturerVO.LoadSum.ToString(),
                                                                     })
                                                {
                                                    LecturerVO = lecturerVO
                                                });
            }
        }

        private void yearNumeric_ValueChanged(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void nameFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void changeDaysload_Click(object sender, EventArgs e)
        {
           // try
            {

            
                if(lecturersListView.SelectedItems.Count == 0)
                    return;

                var selectedLecturer = (lecturersListView.SelectedItems[0] as LecturerListItem).LecturerVO;

                var monthSelect = new SelectMonth();
                var result = monthSelect.ShowDialog();

                if(result != DialogResult.OK)
                    return;



                var form = new EditLecturerLoadDaysForMonth(selectedLecturer, _settings, monthSelect.Month);
                result = form.ShowDialog();

                if(result != DialogResult.OK)
                    return;
            }
       //     catch (Exception)
            {
            }
        }
    }
}
