﻿using System;

namespace StudyLoad.Forms
{
    partial class ShowloadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            _settings.YearChanged -= SettingsOnYearChanged;
            base.Dispose(disposing);
        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShowloadForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.AddButton = new System.Windows.Forms.ToolStripButton();
            this.editButton = new System.Windows.Forms.ToolStripButton();
            this.changeDaysload = new System.Windows.Forms.ToolStripButton();
            this.lecturersListView = new System.Windows.Forms.ListView();
            this.nameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rateHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loadColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.nameFilter = new System.Windows.Forms.TextBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddButton,
            this.editButton,
            this.changeDaysload});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(885, 27);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolstrip3";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // AddButton
            // 
            this.AddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.AddButton.Image = ((System.Drawing.Image)(resources.GetObject("AddButton.Image")));
            this.AddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(80, 24);
            this.AddButton.Text = "Добавить";
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // editButton
            // 
            this.editButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.editButton.Image = ((System.Drawing.Image)(resources.GetObject("editButton.Image")));
            this.editButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(82, 24);
            this.editButton.Text = "Изменить";
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // changeDaysload
            // 
            this.changeDaysload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.changeDaysload.Image = ((System.Drawing.Image)(resources.GetObject("changeDaysload.Image")));
            this.changeDaysload.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.changeDaysload.Name = "changeDaysload";
            this.changeDaysload.Size = new System.Drawing.Size(242, 24);
            this.changeDaysload.Text = "Изменить расспределенные дни";
            this.changeDaysload.Click += new System.EventHandler(this.changeDaysload_Click);
            // 
            // lecturersListView
            // 
            this.lecturersListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lecturersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.rateHeader,
            this.loadColumn});
            this.lecturersListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lecturersListView.FullRowSelect = true;
            this.lecturersListView.Location = new System.Drawing.Point(3, 66);
            this.lecturersListView.MultiSelect = false;
            this.lecturersListView.Name = "lecturersListView";
            this.lecturersListView.Size = new System.Drawing.Size(882, 411);
            this.lecturersListView.Sorting = System.Windows.Forms.SortOrder.Descending;
            this.lecturersListView.TabIndex = 2;
            this.lecturersListView.UseCompatibleStateImageBehavior = false;
            this.lecturersListView.View = System.Windows.Forms.View.Details;
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "Преподователь";
            this.nameHeader.Width = 614;
            // 
            // rateHeader
            // 
            this.rateHeader.Text = "Ставка";
            this.rateHeader.Width = 70;
            // 
            // loadColumn
            // 
            this.loadColumn.Text = "Нагрузка часов";
            this.loadColumn.Width = 162;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(5, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Поиск";
            // 
            // nameFilter
            // 
            this.nameFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameFilter.Location = new System.Drawing.Point(66, 34);
            this.nameFilter.Name = "nameFilter";
            this.nameFilter.Size = new System.Drawing.Size(556, 26);
            this.nameFilter.TabIndex = 7;
            this.nameFilter.TextChanged += new System.EventHandler(this.nameFilter_TextChanged);
            // 
            // ShowloadForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nameFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lecturersListView);
            this.Name = "ShowloadForm";
            this.Size = new System.Drawing.Size(885, 480);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton AddButton;
        private System.Windows.Forms.ToolStripButton editButton;
        private System.Windows.Forms.ListView lecturersListView;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader rateHeader;
        private System.Windows.Forms.ColumnHeader loadColumn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameFilter;
        private System.Windows.Forms.ToolStripButton changeDaysload;
    }
}