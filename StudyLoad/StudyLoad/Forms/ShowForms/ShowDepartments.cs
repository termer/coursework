﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowDepartments : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        private DataManager<DepartmentVO> _departments;
        private DepartmentBUS _departmentBus;
        public ShowDepartments(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
            _departments = new DataManager<DepartmentVO>();
            _departmentBus = new DepartmentBUS();

            UpdateData();
        }

        private class DepartmentListItem : ListViewItem
        {
            public DepartmentVO DepartmentVO;
            public DepartmentListItem(string[] strings)
                : base(strings)
            {

            }
        }

        public void UpdateView()
        {
            departmentsListView.Items.Clear();
            foreach (var subject in _departments.Data.Where(p => p.Name.ToLower().Contains(nameFilter.Text.ToLower())))
            {
                departmentsListView.Items.Add(new DepartmentListItem(new[]
                                                                   {
                                                                       subject.Name,
                                                                   })
                {
                    DepartmentVO = subject
                });
            }
        }

        public void UpdateData()
        {
            _departments.Data.Clear();
            _departments.Add(_departmentBus.GetDepartments(_settings.Year));
            UpdateView();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var subjectsForm = new EditDepartmentForm();
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            _departments.Add(subjectsForm.Department);
            _departments.Update(_departmentBus, _settings.Year);
            UpdateData();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (departmentsListView.SelectedItems.Count == 0)
                return;
            var departmentVO = (departmentsListView.SelectedItems[0] as DepartmentListItem).DepartmentVO;

            var subjectsForm = new EditDepartmentForm(departmentVO);
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            _departments.Update(_departmentBus, _settings.Year);
            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (departmentsListView.SelectedItems.Count == 0)
                return;

            var subject = (departmentsListView.SelectedItems[0] as DepartmentListItem).DepartmentVO;

            _departments.Remove(subject);
            _departments.Update(_departmentBus, _settings.Year);

            UpdateData();
        }

        private void nameFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }
    }
}
