﻿using System;
using System.Linq;
using System.Windows.Forms;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class ShowSubjectsForm : UserControl, IUpdatableListView
    {
        private readonly AppSettings _settings;
        private readonly DataManager<SubjectVO> _subjects;
        private readonly SubjectBUS _subjectBUS;

        public ShowSubjectsForm(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
            
            _subjects = new DataManager<SubjectVO>();
            _subjectBUS = new SubjectBUS(new StudyloadBUS());

            


            UpdateData();
        }

        public void UpdateData()
        {


            var subjects = _subjectBUS.GetSubjects(_settings.Year);
            _subjects.Data.Clear();
            foreach (var subject in subjects)
            {
                _subjects.Add(subject);
            }

            UpdateView();
            Refresh();
        }

        private void UpdateView()
        {
            subjectsListView.Items.Clear();
            foreach (var subject in _subjects.Data.Where(p => p.Name.ToLower().Contains(subNameFilter.Text.ToLower()) && 
                                                              p.Code.ToLower().Contains(subCodeFilter.Text.ToLower())))
            {
                subjectsListView.Items.Add(new SubjectListItem(new[]
                                                                   {
                                                                       subject.Name,
                                                                       subject.Code,
                                                                       subject.Semester1.Overall.ToString(),
                                                                       subject.Semester2.Overall.ToString(),
                                                                       subject.Overall.ToString()
                                                                   })
                                               {
                                                   SubjectVO = subject
                                               });
            }
        }


        private class SubjectListItem : ListViewItem
        {
            public SubjectVO SubjectVO;
            public SubjectListItem(string[] strings)
                :base(strings)
            {

            }
        }


        private void AddButton_Click_1(object sender, EventArgs e)
        {
            var subjectsForm = new EditSubjectForm();
            var result = subjectsForm.ShowDialog(this);

            if (result != DialogResult.OK)
                return;
            _subjects.Add(subjectsForm.Subject);
            _subjects.Update(_subjectBUS, _settings.Year);


            UpdateData();
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;



            var subjectVO = ((SubjectListItem)(subjectsListView.SelectedItems[0])).SubjectVO;

            var dependableLecturers = _subjectBUS.GetDependableLecturers(subjectVO.Id, new LecturerBUS());
            if (dependableLecturers.Count() != 0)
            {
                var deleteDepenForm = new DeleteDependeciesForm(dependableLecturers);
                var result = deleteDepenForm.ShowDialog();

                if (result == DialogResult.Cancel)
                    return;
            }



            _subjects.Remove(subjectVO);
            _subjects.Update(_subjectBUS, _settings.Year);

            UpdateData();
        }

        private void editButton_Click_1(object sender, EventArgs e)
        {
            if (subjectsListView.SelectedItems.Count == 0)
                return;



            var subjectVO = ((SubjectListItem)(subjectsListView.SelectedItems[0])).SubjectVO;
            var editLecturerForm = new EditSubjectForm(subjectVO);
            editLecturerForm.ShowDialog(this);

            _subjects.Update(_subjectBUS, _settings.Year);
            UpdateData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }

        private void subCodeFilter_TextAlignChanged(object sender, EventArgs e)
        {

        }

        private void subCodeFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateView();
        }
    }
}
