﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FastReport;
using System.Xml;
using MySql.Data.MySqlClient;
using StudyLoad.BUS;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class MonthLoadReport : Form
    {
        private readonly AppSettings _settings;
        private readonly Report _report = new Report();
        private bool _emptySet = false;

        public MonthLoadReport(AppSettings settings, LecturerVO lecturer, Month month)
        {
            _settings = settings;
            InitializeComponent();

            SaveMonthLoadToXml(lecturer, month);
        }

        private void MonthLoadForm_Load(object sender, EventArgs e)
        {
            if(_emptySet)
            {
                MessageBox.Show("Данный преподаватель не имеет предметов в нагрузке");
                Close();
                return;
            }

            _report.Load("Reports//MonthLoadReport.frx");
            //_report.SetParameterValue("YearParam", _settings.Year);
            _report.Preview = previewControl1;
            _report.Show();
            //_report.Show();
        }

        private void SaveMonthLoadToXml(LecturerVO lecturer, Month month)
        {
            var daysBUS = new LecturerDatedPairBUS(new StudyloadBUS());
            var pairs = daysBUS.GetDatedPairsForMonth(lecturer, month, _settings.Year);
          //  try
            {

            
                using(var writer = XmlWriter.Create("Reports//MonthLoad.xml"))
                {
                    writer.WriteStartElement("Attr");
                    writer.WriteAttributeString("Month", DateHelper.GetMonthName(month, true));
                    writer.WriteAttributeString("LecturerName", lecturer.FIO);


                    if (lecturer.BindedSubjects.Data.Any())
                    {
                        foreach (var subjectVO in lecturer.BindedSubjects.Data)
                        {
                        writer.WriteStartElement("Element");
                            writer.WriteAttributeString("Subject", subjectVO.Subject.Name);
                            writer.WriteAttributeString("Group", subjectVO.Group.FullName);

                            var sum = pairs.Sum(p => p.Pairs.Sum(t => t.Hours));
                            writer.WriteAttributeString("Sum", sum.ToString());

                            if (pairs.Any())
                            {
                                AddPairs(month, writer, pairs, subjectVO);
                            }
                            else
                            {
                                for (int i = 1; i <= DateTime.DaysInMonth(_settings.Year, (int)month); i++)
                                {
                                    writer.WriteStartElement("Day");
                                    writer.WriteAttributeString("n", i.ToString());
                                    writer.WriteAttributeString("Hours", "0");
                                    writer.WriteEndElement();
                                }
                            }
                            
                        }
                        writer.WriteEndElement();
                    }
                    else
                        _emptySet = true;
                    writer.WriteEndElement();
                }
            }
           // catch
            {
            }
        }

        private void AddPairs(Month month, XmlWriter writer, LecturerDatedPairsVO[] pairs, BindedSubjectVO subjectVO)
        {
            foreach (var p in pairs.Where(p => p.BindedSubjectVO == subjectVO))
            {
                for (int i = 1; i <= DateTime.DaysInMonth(_settings.Year, (int) month); i++)
                {
                    writer.WriteStartElement("Day");
                    writer.WriteAttributeString("n", i.ToString());


                    var datedPair = p.Pairs.FindIndex(a => a.Date.Day == i);
                    writer.WriteAttributeString("Hours", datedPair == -1
                                                             ? "0"
                                                             : p.Pairs[datedPair].Hours.ToString());


                    writer.WriteEndElement();
                }
            }
        }
    }
}
