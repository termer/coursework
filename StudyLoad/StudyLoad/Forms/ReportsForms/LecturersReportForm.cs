﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using FastReport;
using MySql.Data.MySqlClient;
using StudyLoad.BUS;
using StudyLoad.DAO;
using StudyLoad.Properties;
using StudyLoad.VO;

namespace StudyLoad.Forms
{
    public partial class LecturersReportForm : Form
    {
        private readonly AppSettings _settings;
        private readonly Report _report = new Report();

        public LecturersReportForm(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
        }


        private void LecturersReportForm_Load(object sender, EventArgs e)
        {
            SaveLecturersLoadToXml();

            _report.Load("Reports//StudyLoadReport2.frx");
            _report.Preview = previewControl1;
            _report.Show();

            //_report.Load("Reports//StudyLoadReport.frx");
            //_report.SetParameterValue("YearParam", _year);
            //MySqlHelper.OpenConnection();
            //_report.RegisterData(
            //    MySqlHelper.CallProcedure("GetSubjectsForLecturer",
            //                              new[]
            //                                  {
            //                                      new MySqlParameter("y", _settings.Year)
            //                                  }),
            //                              "GetSubjectsForLecturers");
            //_report.GetDataSource("GetSubjectsForLecturers").Enabled = true;
            //MySqlHelper.CloseConnection();
            //_report.RegisterData(new LecturerDAO().GetLecturers(_year), "Lecturers");
            //_report.GetDataSource("Lecturers").Enabled = true;

            //_report.Preview = previewControl1;
           
            //_report.Show();
        }

        private void SaveLecturersLoadToXml()
        {
            var lecturerBUS = new LecturerBUS();
            var studyLoad = new StudyloadBUS();
            
            using (var writer = XmlWriter.Create("Reports//StudyloadReport.xml"))
            {
                writer.WriteStartElement("Attr");
                writer.WriteAttributeString("Year", _settings.Year.ToString());
                foreach (var lecturer in lecturerBUS.GetLecturersWithLoad(_settings.Year))
                {
                    writer.WriteStartElement("Lecturer");
                    writer.WriteAttributeString("FIO", lecturer.FIO);
                    studyLoad.Lecturer = lecturer;
                    foreach (var bindedSubject in lecturer.BindedSubjects.Data)
                    {
                        writer.WriteStartElement("Subject");
                        writer.WriteAttributeString("Group", bindedSubject.Group.FullName);
                        writer.WriteAttributeString("SubjectName", bindedSubject.Subject.Name);
                        writer.WriteAttributeString("LecTime1", bindedSubject.Subject.Semester1.LecTime.ToString());
                        writer.WriteAttributeString("LabTime1", bindedSubject.Subject.Semester1.LabTime.ToString());
                        writer.WriteAttributeString("CourseWork1", bindedSubject.Subject.Semester1.CourseWorkTime.ToString());
                        writer.WriteAttributeString("Practice1", bindedSubject.Subject.Semester1.PracticeTime.ToString());
                        writer.WriteAttributeString("Consultation1", bindedSubject.Subject.Semester1.ConsultationTime.ToString());
                        writer.WriteAttributeString("Examination1", bindedSubject.Subject.Semester1.ExaminationTime.ToString());
                        writer.WriteAttributeString("Credit1", bindedSubject.Subject.Semester1.CreditTime.ToString());
                        writer.WriteAttributeString("Semester1", bindedSubject.Subject.Semester1.OverallTime.ToString());
                        writer.WriteAttributeString("Week1", bindedSubject.Subject.Semester1.WeekTime.ToString());
                        writer.WriteAttributeString("Overall1", bindedSubject.Subject.Semester1.Overall.ToString());

                        writer.WriteAttributeString("LecTime2", bindedSubject.Subject.Semester2.LecTime.ToString());
                        writer.WriteAttributeString("LabTime2", bindedSubject.Subject.Semester2.LabTime.ToString());
                        writer.WriteAttributeString("CourseWork2", bindedSubject.Subject.Semester2.CourseWorkTime.ToString());
                        writer.WriteAttributeString("Practice2", bindedSubject.Subject.Semester2.PracticeTime.ToString());
                        writer.WriteAttributeString("Consultation2", bindedSubject.Subject.Semester2.ConsultationTime.ToString());
                        writer.WriteAttributeString("Examination2", bindedSubject.Subject.Semester2.ExaminationTime.ToString());
                        writer.WriteAttributeString("Credit2", bindedSubject.Subject.Semester2.CreditTime.ToString());
                        writer.WriteAttributeString("Semester2", bindedSubject.Subject.Semester2.OverallTime.ToString());
                        writer.WriteAttributeString("Week2", bindedSubject.Subject.Semester2.WeekTime.ToString());
                        writer.WriteAttributeString("Overall2", bindedSubject.Subject.Semester2.Overall.ToString());

                        writer.WriteAttributeString("DoneTime", studyLoad.GetDoneTimeForYear(bindedSubject, _settings.Year).ToString());

                        writer.WriteEndElement();
                    }
                    writer.WriteEndElement();  
                }
                writer.WriteEndElement();  
            }
        }
    }
}
