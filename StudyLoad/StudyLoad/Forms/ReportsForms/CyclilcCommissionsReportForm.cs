﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using FastReport;
using StudyLoad.BUS;

namespace StudyLoad.Forms
{
    public partial class CyclilcCommissionsReportForm : Form
    {
        private readonly AppSettings _settings;
        private CyclicCommissionBUS _comissionsBUS;
        private readonly Report _report = new Report();

        public CyclilcCommissionsReportForm(AppSettings settings)
        {
            _settings = settings;
            InitializeComponent();
            _comissionsBUS = new CyclicCommissionBUS();
            SaveToXml();
        }



        private void CyclilcCommissionsReportForm_Load(object sender, EventArgs e)
        {
            SaveToXml();
            
            _report.Load("Reports//CyclicCommissions.frx");
            //_report.SetParameterValue("YearParam", _settings.Year);
            _report.Preview = previewControl1;
            _report.Show();
        }

        private void SaveToXml()
        {
            var comissions = _comissionsBUS.GetCyclicCommissions(_settings.Year);

            using (var writer = XmlWriter.Create("CyclicCommissions.xml"))
            {
                writer.WriteStartElement("Doc");
                foreach (var comission in comissions)
                {
                    writer.WriteStartElement("Commission");
                    writer.WriteAttributeString("Name", comission.Name);
                    foreach (var entry in comission.Entries)
                    {
                        writer.WriteStartElement("Entry");
                        writer.WriteAttributeString("FIO", entry.Lecturer.FIO);
                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
        }

    }
}
