﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class DatedMonthBUS:IBUSObject<DatedMonthVO>
    {
        private DatedMonthDAO _datedMonthDAO;

        public DatedMonthBUS()
        {
            _datedMonthDAO = new DatedMonthDAO();
        }

        public DatedMonthVO GetMonth(BindedSubjectVO bindedSubject, Month month)
        {
            var p = _datedMonthDAO.GetMonth(bindedSubject.Id, (int)month);
            if (p.Rows.Count > 1)
                throw new Exception("too many rows");
            if(p.Rows.Count == 0)
                return null;

            DataRow row = p.Rows[0];

            var m = new DatedMonthVO
                        {
                            Status = DataStatus.NotChanged,
                            BindedSubject = bindedSubject,
                            Hours = (int)row["Hours"],
                            Id = (int)row["idDatedMonth"],
                            Month = month
                        };
            return m;
        }

        public int GetMonthHoursForPairs(List<LecturerDayVO> days, Month month, List<DateTime> ignoredDates)
        {
            //var weekFirst = true;

            //while (month != upMonth)
            //{
            //    var daysInMonth = DateTime.DaysInMonth(_settings.Year, (int)month);
            //    var l = new DateTime(_settings.Year, (int)month, 1);


            //    var day = (DayOfWeek)(l.DayOfWeek);

            //    var pairs = new List<LecturerDayVO.Pair>();

            //    for (int i = 1; i <= daysInMonth; i++)
            //    {
            //    }

            //    month++;
            //    if (month == Month.None)
            //        month = Month.January;
            //}

            //return datedPairsVO;
            return 0;
        }

        public int UpdateInDataBase(DatedMonthVO data, int year)
        {
            int id = data.Id;

            switch (data.Status)
            {
                case DataStatus.New:
                    id = _datedMonthDAO.InsertInDb(data.BindedSubject.Id, (int)data.Month, data.Hours);
                    break;
                case DataStatus.Updated:
                    _datedMonthDAO.UpdateInDb(data.Id, data.BindedSubject.Id, (int)data.Month, data.Hours);
                    break; 
                case DataStatus.Removed:
                    _datedMonthDAO.RemoveFromDb(data.Id);
                    id = -1;
                    break;
            }
            return id;
        }
    }
}
