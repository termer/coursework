﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class LecturerDatedPairBUS:IBUSObject<LecturerDatedPairsVO>
    {
        private readonly LecturerDatedPairDAO _lecturerDatedPairDAO;
        private readonly StudyloadBUS _studyLoadBUS;

        public LecturerDatedPairBUS(StudyloadBUS studyloadBUS)
        {
            _lecturerDatedPairDAO = new LecturerDatedPairDAO();
            _studyLoadBUS = studyloadBUS;
        }

        public LecturerDatedPairsVO[] GetDatedPairsForMonth(LecturerVO lecturer, Month month, int year)
        {
            var table = _lecturerDatedPairDAO.GetPairsForMonth(lecturer.Id, (int)month, year);
            //var pairs = new LecturerDatedPairsVO(subject);
            var datedPairs = CreateFromTable(lecturer, table);

            return datedPairs.ToArray();
        }

        public LecturerDatedPairsVO[] GetDatedPairsForYear(LecturerVO lecturer, int year)
        {
            var table = _lecturerDatedPairDAO.GetPairsForYear(lecturer.Id, year);

            var datedPairs = CreateFromTable(lecturer, table);

            return datedPairs.ToArray();
        }

        public LecturerDatedPairsVO[] GetDatedPairsForYearForSubject(LecturerVO lecturer, BindedSubjectVO bindedSubject, int year)
        {
            var table = _lecturerDatedPairDAO.GetPairsForYear(lecturer.Id, year);

            var datedPairs = CreateFromTable(lecturer, table);

            return datedPairs.Where(p => p.BindedSubjectVO.Id == bindedSubject.Id).ToArray();
        }

        public List<LecturerDatedPairsVO> CreateFromTable(LecturerVO lecturer, DataTable table)
        {
            var datedPairs = new List<LecturerDatedPairsVO>(table.Rows.Count/4);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];

                LecturerDatedPairsVO pairs = datedPairs.Where(p => p.BindedSubjectVO != null).ToList().Find(p => p.BindedSubjectVO.Id == (int) row["idStudyload"]);

                if (pairs == null)
                {
                    var bs = lecturer.BindedSubjects.Data.Find(p => p.Id == (int) row["idStudyload"]);
                    pairs = new LecturerDatedPairsVO(bs);
                    datedPairs.Add(pairs);
                }

                AddPairFromRow(pairs, row);
            }
            return datedPairs;
        }



        private void AddPairFromRow(LecturerDatedPairsVO pair, DataRow row)
        {
            pair.SetPair((int)row["Hours"],
                         DateTime.Parse(row["Date"].ToString()),
                         (int) row["idDatedLecturerPair"]);
        }

        public int UpdateInDataBase(LecturerDatedPairsVO data, int year)
        {
            foreach (var pair in data.Pairs)
            {
                switch (pair.Status)
                {
                    case DataStatus.New:
                        _lecturerDatedPairDAO.InsertToDb(pair.Hours,
                                                         pair.Date,
                                                         data.BindedSubjectVO.Id);
                        break;
                    case DataStatus.Updated:
                        _lecturerDatedPairDAO.UpdateInDb(pair.Id,
                                                         pair.Hours,
                                                         pair.Date,
                                                         data.BindedSubjectVO.Id);
                        break;
                    case DataStatus.Removed:
                        _lecturerDatedPairDAO.RemoveFromDb(pair.Id);
                        break;
                }
            }

            return data.Id;
        }
    }
}
