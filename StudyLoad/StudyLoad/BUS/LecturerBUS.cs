﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class LecturerBUS : IBUSObject<LecturerVO>
    {
        private readonly LecturerDAO _lecturerDAO;
        private readonly SubjectBUS _subjectBUS;
        private readonly GroupBUS _groupBUS;
        private readonly StudyloadBUS _studyloadBUS;

        public LecturerBUS()
        {
            _studyloadBUS = new StudyloadBUS();
            _lecturerDAO = new LecturerDAO();
            _subjectBUS = new SubjectBUS(_studyloadBUS);
            
            _groupBUS = new GroupBUS(_studyloadBUS);
            
        }

        public LecturerBUS(LecturerDAO lecturerDAO, SubjectBUS subjectBUS)
        {
            _lecturerDAO = lecturerDAO;
            _subjectBUS = subjectBUS;
        }

        public LecturerVO[] GetLecturers(int year)
        {
            var table = _lecturerDAO.GetLecturers(year);
            return GetLecturers(table);
        }

        public LecturerVO[] GetLecturers(DataTable table)
        {
            var lecturers = new LecturerVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow row in table.Rows)
                lecturers[i++] = CreateLecturerFromRow(row);

            return lecturers;
        }



        public LecturerVO[] GetLecturersWithLoad(int year)
        {
            var table = _lecturerDAO.GetLecturers(year);

            var lecturers = new LecturerVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow row in table.Rows)
            {
                var lecturerVO = CreateLecturerFromRow(row);
                var subjects = _studyloadBUS.GetBindedSubjectsForLecturer(lecturerVO, year);
                lecturerVO.BindedSubjects.Data.AddRange(subjects);

                lecturers[i++] = lecturerVO;
            }

            return lecturers;
        }


        public static LecturerVO CreateLecturerFromRow(DataRow row)
        {
            return new LecturerVO
            {
                Id = int.Parse(row["idLecturer"].ToString()),
                FIO = row["FIO"].ToString(),
                Rate = int.Parse(row["rate"].ToString()),
                Status = DataStatus.NotChanged
            };
        }

        //public void UpdateInDataBase(List<LecturerVO> lecturers)
        //{
        //    foreach (var lecturer in lecturers)
        //        UpdateInDataBaseNoConnection(lecturer);
        //}

        public int UpdateInDataBase(LecturerVO lecturer, int year)
        {
            var id = UpdateLecturer(lecturer, year);
            return id;

        }

        //private void UpdateInDataBaseNoConnection(LecturerVO lecturer)
        //{
        //    UpdateLecturer(lecturer);
        //}

        private int UpdateLecturer(LecturerVO lecturer, int year)
        {
            if (lecturer == null)
                throw new Exception("lecturer null");
            if (lecturer.FIO == "")
                throw new Exception("empty name");
           // if (lecturer.Rate <= 0)
          //      throw new Exception("not valid rate");
            if (year <= 0)
                throw new Exception("NO!");

            var id = 0;
            _studyloadBUS.Lecturer = lecturer;

            switch (lecturer.Status)
            {
                case DataStatus.New:
                    id = _lecturerDAO.InsertToDb(lecturer.FIO, lecturer.Rate, year);
                    lecturer.Id = id;
                    lecturer.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = lecturer.Id;
                    _lecturerDAO.UpdateInDb(id, lecturer.FIO, lecturer.Rate, year);

                    lecturer.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Removed:
                    _studyloadBUS.Lecturer = lecturer;
                    _lecturerDAO.RemoveFromDb(lecturer.Id);
                    id = -1;
                    break;
            }
            lecturer.BindedSubjects.Update(_studyloadBUS, year);
            return id;
        }


    }
}
