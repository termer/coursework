﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    class CyclicCommissionBUS:IBUSObject<CyclicCommissionVO>
    {
        private readonly CyclicCommissionDAO _cyclicCommissionDAO;

        public CyclicCommissionBUS()
        {
            _cyclicCommissionDAO = new CyclicCommissionDAO();
        }

        public CyclicCommissionVO[] GetCyclicCommissions(int year)
        {
            var table = _cyclicCommissionDAO.GetCyclicCommissions(year);
            var cyclicCommissionVos = new List<CyclicCommissionVO>(table.Rows.Count/4);
            var lecturers = new List<LecturerVO>();

            foreach (DataRow row in table.Rows)
            {
                var id = (int) row["idCyclicCommission"];
                CyclicCommissionVO cc;
                if (cyclicCommissionVos.Find(p => p.Id == id) != null)
                    cc = cyclicCommissionVos.Find(p => p.Id == id);
                else
                {
                    cc = new CyclicCommissionVO
                             {
                                 Id = id,
                                 Name = (string) row["Name"],
                                 Status = DataStatus.NotChanged
                             };
                    cyclicCommissionVos.Add(cc);
                }
                var lec = LecturerBUS.CreateLecturerFromRow(row);

                var find = lecturers.Find(p => p.Id == lec.Id);
                if (find == null)
                    lecturers.Add(lec);
                else
                    lec = find;

                cc.Add(lec, DataStatus.NotChanged);
            }
            return cyclicCommissionVos.ToArray();
        }

        public int UpdateInDataBase(CyclicCommissionVO data, int year)
        {
            int id = data.Id;

            switch (data.Status)
            {
                case DataStatus.New:
                    id = _cyclicCommissionDAO.InsertToDB(data.Name, year, data.Entries.Select(p => p.Lecturer.Id).ToArray());
                    break;
                case DataStatus.Updated:
                    _cyclicCommissionDAO.UpdateInDB(data.Id, data.Name, year, data.Entries.Where(p => p.Status == DataStatus.New).Select(p => p.Lecturer.Id).ToArray(),
                                                                              data.Entries.Where(p => p.Status == DataStatus.Removed).Select(p => p.Lecturer.Id).ToArray());
                    break;
                case DataStatus.Removed:
                    _cyclicCommissionDAO.RemoveFromDB(data.Id);
                    id = -1;
                    break;
            }

            return id;

        }
    }
}
