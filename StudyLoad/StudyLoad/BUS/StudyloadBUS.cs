﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class StudyloadBUS: IBUSObject<BindedSubjectVO>
    {
        public LecturerVO Lecturer;

        private StudyloadDAO _studyloadDAO;
        private SubjectBUS _subjectBUS;
        private GroupBUS _groupBUS;

        private GroupDAO _groupDAO;
        private SubjectDAO _subjectDAO;
        private readonly LecturerDatedPairBUS _daysBUS;


        public StudyloadBUS()
        {
            _studyloadDAO = new StudyloadDAO();

            _groupBUS = new GroupBUS(this);
            _subjectBUS = new SubjectBUS(this);

            _groupDAO = new GroupDAO();
            _subjectDAO = new SubjectDAO();

            _daysBUS = new LecturerDatedPairBUS(this);
        }

        public int UpdateInDataBase(BindedSubjectVO data, int year)
        {
            var id = data.Id;

            switch (data.Status)
            {
                case DataStatus.New:
                    id = _studyloadDAO.InsertToDB(Lecturer.Id, data.Group.Id, data.Subject.Id, year);

                    data.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = data.Id;
                    _studyloadDAO.UpdateInDb(data.Id, Lecturer.Id, data.Group.Id, data.Subject.Id, data.Year);
                    UpdatePairs(id, data.GetDays());
                    data.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Removed:
                    foreach (var dayVO in data.GetDays())
                    {
                        dayVO.Status = DataStatus.Removed;
                        foreach (var pair in dayVO.GetPairs())
                        {
                            pair.Status = DataStatus.Removed;
                        }
                    }

                    
                    _studyloadDAO.RemoveFromDb(data.Id);
                    id = -1;
                    break;
            }

            if(data.Status != DataStatus.Removed)
                UpdatePairs(id, data.GetDays());

            return id;
        }
        private void UpdatePairs(int idstudyload, List<LecturerDayVO> days)
        {

            foreach (var day in days)
            {
                if (!day.PairsChanged() && day.Status != DataStatus.NotChanged)
                    continue;
                
                foreach (var pair in day.GetPairs())
                {
                    if (pair.Status == DataStatus.New)
                    {
                        _studyloadDAO.InsertPair(idstudyload, (int) day.Day, day.Semester , pair.Hours, (int) pair.PairState);
                    }
                    else if (pair.Status == DataStatus.Updated ||
                             day.Status == DataStatus.Updated)
                    {
                        _studyloadDAO.UpdatePair(pair.Id, idstudyload, (int)day.Day, day.Semester , pair.Hours, (int)pair.PairState);
                    }
                    else if (pair.Status == DataStatus.Removed)
                    {
                        _studyloadDAO.RemovePairFromDb(pair.Id);
                    }

                    pair.Status = DataStatus.NotChanged;
                }

                var toremove = day.GetPairs().Where(p => p.Status == DataStatus.Removed);
                foreach (var pair in toremove)
                {
                    day.RemovePairFromList(pair);
                }

                day.Status = DataStatus.NotChanged;
            }
        }

        public LecturerDatedPairsVO GetDatedPairs(int semester, BindedSubjectVO bindedSubject, int year)
        {
            var month = semester == 1 ? Month.September : Month.January;
            var upMonth = semester == 1 ? Month.January : Month.June;


            var weekFirst = true;

            var datedPairsVO = _daysBUS.GetDatedPairsForYear(Lecturer, year).FirstOrDefault(s => s.BindedSubjectVO == bindedSubject) ??
                               new LecturerDatedPairsVO(bindedSubject);


            while (month != upMonth)
            {
                weekFirst = AddDatedPairsForMonth(bindedSubject, year, weekFirst, datedPairsVO, month);

                month++;
                if (month == Month.None)
                    month = Month.January;
            }

            return datedPairsVO;
        }

        private static bool AddDatedPairsForMonth(BindedSubjectVO bindedSubject, int year, bool weekFirst, LecturerDatedPairsVO datedPairsVO,
                                      Month month, bool ignoreOld = true)
        {
            var daysInMonth = DateTime.DaysInMonth(year, (int) month);
            var l = new DateTime(year, (int) month, 1);


            var day = (DayOfWeek) (l.DayOfWeek);

            var pairs = new List<LecturerDayVO.Pair>();

            for (int i = 1; i <= daysInMonth; i++)
            {
                var d = bindedSubject.GetDays().Find(p => p.Day == day);
                if (d != null)
                {
                    pairs.AddRange(d.GetPairs().Where(p => p.PairState == LecturerDayVO.Pair.LecturerPairState.Usual ||
                                                           (weekFirst &&
                                                            p.PairState == LecturerDayVO.Pair.LecturerPairState.WeekOne) ||
                                                           (!weekFirst &&
                                                            p.PairState == LecturerDayVO.Pair.LecturerPairState.WeekTwo)));
                }

                var date = new DateTime(year, (int) month, i);

                var sum = 0; 

                if (!ignoreOld)
                    sum = pairs.Sum(p => p.Hours);
                else
                {
                    pairs.Where(p => p.Status == DataStatus.New).Sum(p => p.Hours);
                    sum -= pairs.Where(p => p.Status == DataStatus.Removed).Sum(p => p.Hours);
                }

                if (sum != 0)
                    datedPairsVO.AddPair(sum, date);

                day++;
                if (day == DayOfWeek.None)
                {
                    weekFirst = !weekFirst;
                    day = DayOfWeek.Monday;
                }
                pairs.Clear();
            }
            return weekFirst;
        }

        public int CalculateTimeForMonth(int year, Month month, BindedSubjectVO bindedSubject)
        {
            Month currentMonth = Month.September;
            bool firstWeek = false;
            int day = 1;
            do
            {

                var daysInMonth = DateTime.DaysInMonth(year, (int)currentMonth) - day;

                var monthFullWeeks =  daysInMonth/7;

                day = daysInMonth - (monthFullWeeks*7);

                if (monthFullWeeks % 2 != 0)
                    firstWeek = !firstWeek;


                currentMonth++;
                if(currentMonth > Month.December)
                    currentMonth = Month.January;
            } while (currentMonth <= month) ;
            var datedPairs = new LecturerDatedPairsVO(bindedSubject);

            AddDatedPairsForMonth(bindedSubject, year, firstWeek, datedPairs, month, false );


            return datedPairs.Pairs.Sum(p =>p.Hours);
        }
        public int CalculateTimeForYear(int year, BindedSubjectVO bindedSubject)
        {
            return new[] 
            {
                Month.January,
                Month.February,
                Month.March,
                Month.April,
                Month.May,
                Month.June,
                Month.July,
                Month.August,
                Month.September,
                Month.October,
                Month.November,
                Month.December
            }.Sum(p => CalculateTimeForMonth(year, p, bindedSubject));
        }

        public int GetDoneTimeForYear(BindedSubjectVO bindedSubject, int year)
        {
            return _daysBUS.GetDatedPairsForYearForSubject(Lecturer, bindedSubject, year).Sum(p => p.Pairs.Sum(p2 => p2.Hours));
        }


        public BindedSubjectVO[] GetBindedSubjectsForLecturer(LecturerVO lecturer, int year)
        {
            var table = _studyloadDAO.GetSubjectsForLecturer(lecturer.Id, year);
            return GetBindedSubjects(table);
        }

        public BindedSubjectVO[] GetBindedSubjectsForGroup(GroupVO group)
        {
            var table = _studyloadDAO.GetSubjectsForGroup(group.Id);
            return GetBindedSubjects(table);
        }

        public BindedSubjectVO[] GetBindedSubjectsForSubject(SubjectVO subject)
        {
            var table = _studyloadDAO.GetBindedSubjectsForSubject(subject.Id);
            return GetBindedSubjects(table);
        }

        private BindedSubjectVO[] GetBindedSubjects(DataTable table)
        {
            var bindedSubjects = new BindedSubjectVO[table.Rows.Count];

            for (var i = 0; i < table.Rows.Count; i++)
                bindedSubjects[i] = GetBindedSubject(table.Rows[i]);

            return bindedSubjects;
        }

        public BindedSubjectVO GetBindedSubject(DataRow row)
        {
            var bindedSubject = new BindedSubjectVO
                                    {
                                        Group = _groupBUS.GetGroupWithId((int)row["idGroup"]),
                                        Subject = _subjectBUS.GetSubjectWithId((int)row["idSubject"]),
                                        Id = (int) row["idStudyload"],
                                        Status = DataStatus.NotChanged
                                    };

            var pairs = _studyloadDAO.GetPairs(bindedSubject.Id);
            foreach (DataRow r in pairs.Rows)
            {
                var day = (DayOfWeek)((int)r["Day"]);
                var semester = (int) r["Semester"];

                var dayVO = bindedSubject.GetDays().Find(p => p.Day == day && p.Semester == semester) ?? new LecturerDayVO{Day = day, Semester = semester};

                dayVO.AddPair(new LecturerDayVO.Pair
                                  {
                                      Hours = (int)r["Hours"],
                                      Id = (int)r["idLecturerPair"],
                                      PairState = (LecturerDayVO.Pair.LecturerPairState)r["WeekType"],
                                      Status = DataStatus.NotChanged
                                  });

                if(!bindedSubject.GetDays().Contains(dayVO))
                    bindedSubject.AddDay(dayVO);
            }


            if (bindedSubject.Group == null)
                throw new Exception("empty group");
            if (bindedSubject.Subject == null)
                throw new Exception("empty subject");

            return bindedSubject;
        }
    }
}
