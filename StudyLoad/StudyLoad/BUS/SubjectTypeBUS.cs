﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    class SubjectTypeBUS:IBUSObject<SubjectTypeVO>
    {
        private SubjectTypeDAO _subjectTypeDAO;

        public SubjectTypeBUS()
        {
            _subjectTypeDAO = new SubjectTypeDAO();
        }

        public SubjectTypeVO[] GetSubjectsTypes()
        {
           return (from DataRow row in _subjectTypeDAO.GetSubjectTypes().Rows
                   select CreateSubjectTypeFromRow(row)).ToArray();
        }

        private SubjectTypeVO CreateSubjectTypeFromRow(DataRow row)
        {
            return new SubjectTypeVO
                       {
                           Id = int.Parse(row["idSubjectType"].ToString()),
                           Name = row["subjectTypeName"].ToString(),
                           Status = DataStatus.NotChanged
                       };
        }

        public int UpdateInDataBase(SubjectTypeVO subjectType, int year)
        {
            int id = -1;
            switch (subjectType.Status)
            {
                case DataStatus.New:
                    id = _subjectTypeDAO.InsertToDB(subjectType.Name);
                    subjectType.Id = id;

                    subjectType.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = subjectType.Id;
                    _subjectTypeDAO.UpdateInDB(id, subjectType.Name);

                    subjectType.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Removed:
                    var subjectBUS = new SubjectBUS(new StudyloadBUS());
                    var subjects = subjectBUS.GetSubjects(year);
                    foreach (var subjectVO in subjects.Where(p => p.SubjectType.Id == subjectType.Id))
                    {
                        subjectVO.Status = DataStatus.Removed;
                        subjectBUS.UpdateInDataBase(subjectVO, year);
                    }

                    _subjectTypeDAO.RemoveFromDB(subjectType.Id);
                    id = -1;
                    break;
            }
            return id;
        }
    }
}
