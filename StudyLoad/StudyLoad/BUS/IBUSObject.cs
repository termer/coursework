﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public interface IBUSObject<T> where T: IVObject
    {
        int UpdateInDataBase(T data, int year);
    }
}
