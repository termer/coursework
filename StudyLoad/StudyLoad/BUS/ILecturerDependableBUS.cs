﻿using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public interface ILecturerDependableBUS
    {
        LecturerVO[] GetDependableLecturers(int id, LecturerBUS lecturerBUS);
    }
}
