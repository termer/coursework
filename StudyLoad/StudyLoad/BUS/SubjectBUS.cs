﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class SubjectBUS:IBUSObject<SubjectVO>, ILecturerDependableBUS
    {
        private readonly SubjectDAO _subjectDAO;
        private StudyloadBUS _studyloadBUS;

        public SubjectBUS(StudyloadBUS studyloadBUS)
        {
            _subjectDAO = new SubjectDAO();
            _studyloadBUS = studyloadBUS;
        }

        public SubjectBUS(SubjectDAO subjectDAO)
        {
            _subjectDAO = subjectDAO;
        }

        public SubjectVO[] GetSubjects(int year)
        {
            var table = _subjectDAO.GetSubjects(year);

            var subjects = new SubjectVO[table.Rows.Count];

            for (int i = 0; i < table.Rows.Count; i++)
                subjects[i] = GetSubject(table.Rows[i]);

            return subjects;
        }

        public SubjectVO GetSubject(DataRow row)
        {
            var subject = new SubjectVO(DataStatus.NotChanged)
            {
                Id = (int)row["idSubject"],
                Name = row["subjectName"].ToString(),
                Code = row["code"].ToString(),
            };

            int rowId = (int)row["idSubjectType"];
            subject.SubjectType = new SubjectTypeBUS().GetSubjectsTypes().First(p => p.Id == rowId);

            LoadSemesterData(_subjectDAO.GetSemester(subject.Id, 1).Rows[0], ref subject.Semester1);
            LoadSemesterData(_subjectDAO.GetSemester(subject.Id, 2).Rows[0], ref subject.Semester2);

            return subject;
        }

        public int UpdateInDataBase(SubjectVO subject, int year)
        {
            if (subject == null)
                throw new ArgumentNullException("subject");
            if (subject.Name == null)
                throw new ArgumentNullException("subject name");
            if (subject.Code == null)
                throw new ArgumentNullException("subject code");
            if (subject.Overall == 0)
                throw new ArgumentException("overall 0");

            var s1 = subject.Semester1;
            var s2 = subject.Semester2;

            var id = 0;

            switch (subject.Status)
            {
                case DataStatus.New:
                    id = _subjectDAO.InsertSubjectToDb(subject.Name, subject.Code, subject.SubjectType.Id,
                                                      s1.WeekTime, s1.LecTime, s1.LabTime, s1.CourseWorkTime, s1.ConsultationTime,
                                                      s1.ExaminationTime, s1.CreditTime, s1.PracticeTime, s1.OverallTime,
                                                      s2.WeekTime, s2.LecTime, s2.LabTime, s2.CourseWorkTime, s2.ConsultationTime,
                                                      s2.ExaminationTime, s2.CreditTime, s2.PracticeTime, s2.OverallTime, year);
                    subject.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = subject.Id;


                    _subjectDAO.UpdateSubjectInDb(subject.Id, subject.Name, subject.Code, subject.SubjectType.Id,
                                                      s1.Id, s1.WeekTime, s1.LecTime, s1.LabTime, s1.CourseWorkTime, s1.ConsultationTime,
                                                      s1.ExaminationTime, s1.CreditTime, s1.PracticeTime, s1.OverallTime,
                                                      s2.Id, s2.WeekTime, s2.LecTime, s2.LabTime, s2.CourseWorkTime, s2.ConsultationTime,
                                                      s2.ExaminationTime, s2.CreditTime, s2.PracticeTime, s2.OverallTime, year);
                    subject.Status = DataStatus.NotChanged;

                    break;
                case DataStatus.Removed:
                    id = -1;

                    var bindedSubjects = _studyloadBUS.GetBindedSubjectsForSubject(subject);

                    foreach (var bindedSubjectVO in bindedSubjects)
                    {
                        bindedSubjectVO.Status = DataStatus.Removed;
                        _studyloadBUS.UpdateInDataBase(bindedSubjectVO, year);
                    }

                    _subjectDAO.RemoveSubjectFromDB(subject.Id, subject.Semester1.Id, subject.Semester2.Id);
                    break;
            }
            return id;
        }

        private void LoadSemesterData(DataRow row, ref SubjectVO.Semester semester)
        {
            semester.Id = (int) row["idSemester"];
            semester.LecTime = (int) row["lecTime"];
            semester.LabTime = (int) row["labTime"];
            semester.PracticeTime = (int) row["practiceTime"];
            semester.ExaminationTime = (int) row["examinationTime"];
            semester.CreditTime = (int) row["creditTime"];
            semester.ConsultationTime = (int) row["consultationTime"];
            semester.CourseWorkTime = (int) row["courseWorkTime"];
            semester.WeekTime = (int)row["weekTime"];
            semester.OverallTime = (int)row["overallTime"];
        }


        public SubjectVO GetSubjectWithId(int id)
        {
            var table = _subjectDAO.GetSubjectWithId(id);

            if (table.Rows.Count == 0)
                return null;

            return GetSubject(table.Rows[0]);
        }

        public LecturerVO[] GetDependableLecturers(int id, LecturerBUS lecturerBUS)
        {
            var table = _subjectDAO.GetDependableLecturers(id);

            var lecturers = lecturerBUS.GetLecturers(table);

            return lecturers;
        }
    }
}
