﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    class DepartmentBUS:IBUSObject<DepartmentVO>
    {
        private DepartmentDAO _subjectTypeDAO;

        public DepartmentBUS()
        {
            _subjectTypeDAO = new DepartmentDAO();
        }

        public DepartmentVO[] GetDepartments(int year)
        {
           return (from DataRow row in _subjectTypeDAO.GetDepartments(year).Rows
                   select CreateDepartmentFromRow(row)).ToArray();
        }

        public DepartmentVO CreateDepartmentFromRow(DataRow row)
        {
            return new DepartmentVO
                       {
                           Id = int.Parse(row["idDepartment"].ToString()),
                           Name = row["departmentName"].ToString(),
                           SemesterHours = (int)row["SemesterHours"],
                           Status = DataStatus.NotChanged
                       };
        }

        public int UpdateInDataBase(DepartmentVO department, int year)
        {
            int id = -1;
            switch (department.Status)
            {
                case DataStatus.New:
                    id = _subjectTypeDAO.InsertToDB(department.Name, department.SemesterHours, year);
                    department.Id = id;

                    department.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = department.Id;
                    _subjectTypeDAO.UpdateInDB(id, department.Name, department.SemesterHours, year);

                    department.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Removed:
                    var groupBUS = new GroupBUS(new StudyloadBUS());
                    var groups = groupBUS.GetGroups(year).Where(p => p.Department.Id == department.Id);
                    foreach (var groupVO in groups)
                    {
                        groupVO.Status = DataStatus.Removed;
                        groupBUS.UpdateInDataBase(groupVO, year);
                    }


                    _subjectTypeDAO.RemoveFromDB(department.Id);
                    id = -1;
                    break;
            }
            return id;
        }
    }
}
