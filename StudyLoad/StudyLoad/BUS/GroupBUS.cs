﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using StudyLoad.DAO;
using StudyLoad.DataHolders;
using StudyLoad.VO;

namespace StudyLoad.BUS
{
    public class GroupBUS : IBUSObject<GroupVO>, ILecturerDependableBUS
    {
        private readonly GroupDAO _groupDAO;
        private readonly DepartmentBUS _departmentBus;
        private readonly StudyloadBUS _studyloadBUS;

        public GroupBUS(StudyloadBUS studyLoadBUS)
        {
            _groupDAO = new GroupDAO();
            _departmentBus = new DepartmentBUS();
            _studyloadBUS = studyLoadBUS;
        }

        public GroupBUS(StudyloadBUS studyLoadBUS, GroupDAO groupDAO)
        {
            _groupDAO = groupDAO;
            _studyloadBUS = studyLoadBUS;

        }

        public GroupVO[] GetGroups(int year)
        {
            var groupTable = _groupDAO.GetGroups(year);

            var groupArray = new GroupVO[groupTable.Rows.Count];

            for (int i = 0; i < groupTable.Rows.Count; i++)
                groupArray[i] = GetGroup(groupTable.Rows[i]); ;

            return groupArray;
        }

        public GroupVO GetGroup(DataRow row)
        {
            var group = new GroupVO();

            group.Status = DataStatus.NotChanged;
            group.Id = (int)row["idGroup"];
            group.AdmissionYear = (int)row["admissionYear"];
            group.BudgetCount = (int)row["budgetCount"];
            group.ContractCount = (int)row["contractCount"];
            group.Number = row["groupNum"].ToString();

            group.Department = _departmentBus.CreateDepartmentFromRow(row);
            return group;
        }

        public int UpdateInDataBase(GroupVO group, int year)
        {
            if(group == null)
                throw new NullReferenceException("group");
            if(string.IsNullOrEmpty(group.Number))
                throw new NullReferenceException("group.Number");
            if(group.StudentsCount == 0)
                throw  new ArgumentException("group.StudentsCount");
            if (group.Department.Id <= 0)
                throw new ArgumentException("Department.Id");

            var id = UpdateGroup(group, year);
            
            return id;
        }

        private int UpdateGroup(GroupVO group, int year)
        {
            var id = 0;
            switch (group.Status)
            {
                case DataStatus.New:
                    id = _groupDAO.InsertToDB(group.Number, group.Department.Id, group.AdmissionYear, group.BudgetCount,
                                              group.ContractCount, year);
                    group.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Updated:
                    id = group.Id;
                    _groupDAO.UpdateInDB(group.Id, group.Number, group.Department.Id, group.AdmissionYear,
                                         group.BudgetCount, group.ContractCount, year);
                    group.Status = DataStatus.NotChanged;
                    break;
                case DataStatus.Removed:
                    var bindedSubjects = _studyloadBUS.GetBindedSubjectsForGroup(group);

                    foreach (var bindedSubjectVO in bindedSubjects)
                    {
                        bindedSubjectVO.Status = DataStatus.Removed;
                        _studyloadBUS.UpdateInDataBase(bindedSubjectVO, year);
                    }

                    id = -1;

                    _groupDAO.RemoveInDB(group.Id);
                    group.Status = DataStatus.Removed; 
                    break;
            }
            return id;
        }

        public GroupVO GetGroupWithId(int id)
        {
            var table = _groupDAO.GetGroupWithId(id);
            if (table.Rows.Count == 0)
                return null;

            Debug.Assert(table.Rows.Count == 1, "Too many rows");

            return GetGroup(table.Rows[0]);
        }

        public LecturerVO[] GetDependableLecturers(int id, LecturerBUS lecturerBUS)
        {
            var table = _groupDAO.GetDependableLecturers(id);

            var lecturers = lecturerBUS.GetLecturers(table);

            return lecturers;
        }
    }
}
