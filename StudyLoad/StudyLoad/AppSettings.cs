﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace StudyLoad
{
    public class AppSettings
    {
        private int _year;
        public int Year
        {
            get { return _year; }
            set
            {
                _year = value;
                if (YearChanged != null)
                    YearChanged(this, EventArgs.Empty);
            }
        }

        public event EventHandler YearChanged; 

        public AppSettings()
        {
            Year = 2000;
        }

        public void Save(string path)
        {
            using(var writer = XmlWriter.Create(path))
            {
                writer.WriteStartElement("Settings");

                writer.WriteAttributeString("Year", Year.ToString());

                writer.WriteEndElement();

            }
        }
        public void Load(string path)
        {
            if (!File.Exists(path))
                return;

            var xmlDocument = new XmlDocument();
            xmlDocument.Load(path);

            XmlNode node = xmlDocument.SelectSingleNode("Settings");

            Year = int.Parse(node.Attributes["Year"].Value);
        }
    }
}
