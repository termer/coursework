﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{


    public class LecturerDayVO:IVObject
    {
        public class Pair:IVObject
        {
            public enum LecturerPairState
            {
                Usual,
                WeekOne,
                WeekTwo
            }

            private LecturerPairState _pairState;
            private DataStatus _status;
            private int _hours;

            

            public int Hours
            {
                get { return _hours; }
                set
                {
                    FunctionHelper.Set(_hours, value, ref _status);
                    if(value == 0)
                        _status = DataStatus.Removed;

                    _hours = value;
                }
            }

            public LecturerPairState PairState
            {
                get { return _pairState; }
                set
                {
                    FunctionHelper.Set(_pairState, value, ref _status);
                    _pairState = value;
                }
            }

            public DataStatus Status
            {
                get { return _status; }
                set { _status = value; }
            }

            public int Id { get; set; }
        }
        
        private DayOfWeek _day;
        private DataStatus _status;

        private List<Pair> _pairs = new List<Pair>();
        private int _semester;

        public DayOfWeek Day
        {
            get { return _day; }
            set
            {
                FunctionHelper.Set(_day, value, ref _status);
                _day = value;
            }
        }

        public int Semester
        {
            get { return _semester; }
            set
            {
                FunctionHelper.Set(_semester, value, ref _status);
                _semester = value;
            }
        }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; 
            
            }
        }

        public int Id { get; set; }



        public bool PairsChanged()
        {
            return _pairs.Any(p => p.Status != DataStatus.NotChanged);
        }


        public void AddPair(Pair pair)
        {
            if(_pairs.Contains(pair))
                throw new ArgumentException("Already there");

            _pairs.Add(pair);
            FunctionHelper.DataWasUpdated(ref _status);
        }

        public void RemovePairFromList(Pair pair)
        {
            if(!_pairs.Contains(pair))
                throw new ArgumentException("no such pair here");

            _pairs.Remove(pair);
            FunctionHelper.DataWasUpdated(ref _status);
        }


        public void RemovePair(Pair pair)
        {
            if (!_pairs.Contains(pair))
                throw new ArgumentException("no such pair here");

            pair.Status = DataStatus.Removed;
            
            FunctionHelper.DataWasUpdated(ref _status);
        }


        public void ClearPairs()
        {
            _pairs.Clear();
            FunctionHelper.DataWasUpdated(ref _status);
        }

        public List<Pair> GetPairs()
        {
            return _pairs.ToList();
        }
    }
}
