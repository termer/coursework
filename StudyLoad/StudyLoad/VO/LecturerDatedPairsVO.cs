﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class LecturerDatedPairsVO : IVObject
    {


        public struct DatedPair
        {
            public DateTime Date;
            public int Hours;
            public int Id;
            public DataStatus Status;

        }

        public int Id { get; set; }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        //public LecturerVO Lecturer { get; private set; }
        public BindedSubjectVO BindedSubjectVO;

        public List<DatedPair> Pairs;
        private DataStatus _status;

        public LecturerDatedPairsVO(BindedSubjectVO bindedSubject)
        {
            BindedSubjectVO = bindedSubject;
            Pairs = new List<DatedPair>();
        }

        public void SetPair(int hours, DateTime dateTime, int id = -1)
        {
            var i = Pairs.FindIndex(p => p.Date == dateTime);
            if (id == -1 && i != -1)
                id = Pairs[i].Id;

            var pair = new DatedPair
                           {
                               Date = dateTime,
                               Hours = hours
                           };
            if (id != -1)
            {
                pair.Id = id;
                pair.Status = DataStatus.NotChanged;
            }


            if (i != -1)
            {
                if (Pairs[i].Date != dateTime || Pairs[i].Hours != hours)
                    pair.Status = DataStatus.Updated;
                if (pair.Hours == 0)
                    pair.Status = DataStatus.Removed;

                Pairs[i] = pair;
            }
            else
            {
                if (id == -1)
                    pair.Status = DataStatus.New;
                Pairs.Add(pair);
                FunctionHelper.DataWasUpdated(ref _status);
            }
        }

        public void AddPair(int hours, DateTime dateTime)
        {
            var i = Pairs.FindIndex(p => p.Date == dateTime);

            DatedPair pair;

            pair = i != -1
                       ? Pairs[i]
                       : new DatedPair{Date = dateTime};

            pair.Hours += hours;

            pair.Status = pair.Hours > 0
                              ? (i == -1
                                     ? DataStatus.New
                                     : DataStatus.Updated)
                              : DataStatus.Removed;

            if (i == -1 && pair.Hours > 0)
                Pairs.Add(pair);
            else if(i != -1)
                Pairs[i] = pair;
        }
    }
}
