﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class SubjectTypeVO:IVObject
    {

        private string _name;
        private DataStatus _status;
        
        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public int Id { get; set; }


        public string Name 
        {
            get { return _name; }
            set
            {
                FunctionHelper.Set(_name, value, ref _status);
                _name = value;
            }
        }
    }
}
