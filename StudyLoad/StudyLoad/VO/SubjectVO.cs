﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class SubjectVO : IVObject
    {
        public struct Semester
        {
            
            private int _labTime;
            private int _lecTime;
            private int _courseWorkTime;
            private int _consultationTime;
            private int _examinationTime;
            private int _creditTime;
            private int _practiceTime;


            private DataStatus _status;
            private int _id;
            private int _weekTime;
            private int _overallTime;


            public DataStatus Status
            {
                get { return _status; }
                set { _status = value; }
            }


            public int WeekTime
            {
                get { return _weekTime; }
                set
                {
                    FunctionHelper.Set(_weekTime, value, ref _status);
                    _weekTime = value;
                }
            }

            public int LabTime
            {
                get { return _labTime; }
                set
                {
                    FunctionHelper.Set(_labTime, value, ref _status);
                    _labTime = value;
                }
            }

            public int LecTime
            {
                get { return _lecTime; }
                set
                {
                    FunctionHelper.Set(_lecTime, value, ref _status);
                    _lecTime = value;
                }
            }

            public int CourseWorkTime
            {
                get { return _courseWorkTime; }
                set
                {
                    FunctionHelper.Set(_courseWorkTime, value, ref _status);
                    _courseWorkTime = value;
                }
            }

            public int ConsultationTime
            {
                get { return _consultationTime; }
                set
                {
                    FunctionHelper.Set(_consultationTime, value, ref _status);
                    _consultationTime = value;
                }
            }

            public int ExaminationTime
            {
                get { return _examinationTime; }
                set
                {
                    FunctionHelper.Set(_examinationTime, value, ref _status);
                    _examinationTime = value;
                }
            }

            public int CreditTime
            {
                get { return _creditTime; }
                set
                {
                    FunctionHelper.Set(_creditTime, value, ref _status);
                    _creditTime = value;
                }
            }

            public int PracticeTime
            {
                get { return _practiceTime; }
                set
                {
                    FunctionHelper.Set(_practiceTime, value, ref _status);
                    _practiceTime = value;
                }
            }

            public int OverallTime
            {
                get { return _overallTime; }
                set
                {
                    FunctionHelper.Set(_overallTime, value, ref _status);
                    _overallTime = value;
                }
            }


            public int Overall
            {
                get
                {
                    return LecTime + LabTime + CourseWorkTime + PracticeTime + CreditTime + ConsultationTime + ExaminationTime;
                }
            }

            public int Id
            {
                get { return _id; }
                set { _id = value; }
            }

            public Semester(DataStatus status = DataStatus.New)
            {
                _lecTime = 0;
                _labTime = 0;
                _courseWorkTime = 0;
                _practiceTime = 0;
                _examinationTime = 0;
                _consultationTime = 0;
                _creditTime = 0;
                _weekTime = 0;
                _status = status;
                _id = 0;
                _overallTime = 0;
            }
        }

        private int _id;
        private string _name;
        private string _code;
        private SubjectTypeVO _subjectType;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                var status = Status;
                FunctionHelper.Set(_name, value, ref status);
                Status = status;
                _name = value;
            }
        }

        public string Code
        {
            get { return _code; }
            set
            {
                var status = Status;
                FunctionHelper.Set(_code, value, ref status);
                _code = value;
                Status = status;
            }
        }
        public SubjectTypeVO SubjectType
        {
            get { return _subjectType; }
            set
            {
                var status = Status;
                FunctionHelper.Set(_subjectType, value, ref status);
                _subjectType = value;
                Status = status;
            }
        }


        public DataStatus Status
        {
            get
            {
                if (Semester1.Status == DataStatus.Updated || Semester2.Status == DataStatus.Updated)
                    return DataStatus.Updated;

                if (Semester1.Status == DataStatus.New)
                    return DataStatus.New;

                if (Semester1.Status == DataStatus.NotChanged)
                    return DataStatus.NotChanged;

                return DataStatus.Removed;
            }
            set
            {
                
                Semester1.Status = value;
                Semester2.Status = value;
            }
        }

        public int Overall
        {
            get { return Semester1.Overall + Semester2.Overall; }
        }
        
        public Semester Semester1, Semester2;

        public SubjectVO()
        {
            Semester1 = new Semester(DataStatus.New);
            Semester2 = new Semester(DataStatus.New);
        }

        public SubjectVO(DataStatus status)
        {
            Semester1 = new Semester(status);
            Semester2 = new Semester(status);
        }
    }
}
