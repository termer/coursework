﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class CyclicCommissionVO : IVObject
    {
        public struct Entry
        {
            public LecturerVO Lecturer;
            public DataStatus Status;
        }

        private DataStatus _status;
        private string _name;

        public int Id { get; set; }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                FunctionHelper.Set(_name, value, ref _status);
                _name = value;
            }
        }

        private List<Entry> _entries = new List<Entry>();

        public List<Entry> Entries
        {
            get { return _entries.ToList(); }
        }

        public void Add(LecturerVO[] lecturerVOs, DataStatus status)
        {
            foreach (var vo in lecturerVOs)
            {
                Add(vo, status);
            }
        }

        public void Add(LecturerVO lecturerVO, DataStatus status)
        {
            var entry = new Entry
                            {
                                Lecturer = lecturerVO,
                                Status = status
                            };
            _entries.Add(entry);
        }

        public void Remove(LecturerVO lecturer)
        {
            var i = _entries.FindIndex(p => p.Lecturer == lecturer);
            if (i == -1)
                return;
            var entry = _entries[i];
            FunctionHelper.DataWasUpdated(ref entry.Status);
            _entries[i] = entry;
        }
    }
}
