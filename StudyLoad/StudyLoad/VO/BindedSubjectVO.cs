﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.BUS;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class BindedSubjectVO:IVObject
    {
        private GroupVO _group;
        private SubjectVO _subject;
        private DataStatus _status;
        private int _year;

        public SubjectVO Subject
        {
            get { return _subject; }
            set
            {
                FunctionHelper.Set(_subject, value, ref _status);
                _subject = value;
            }
        }

        public GroupVO Group
        {
            get { return _group; }
            set
            {
                FunctionHelper.Set(_group, value, ref _status);
                _group = value;
            }
        }

        public int Year
        {
            get
            {
                return _year;
            }
            set
            {
                FunctionHelper.Set(_year, value, ref _status);
                _year = value;
            }
        }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private List<LecturerDayVO> _days = new List<LecturerDayVO>(); 

        public int Id { get; set; }

        public BindedSubjectVO()
        {

        }

        public void AddDay(LecturerDayVO day)
        {
            if(_days.Contains(day))
                return;
            if(_days.FirstOrDefault(p => p.Day == day.Day && p.Semester == day.Semester) != null)
                throw new ArgumentException("same day is alredy here");

            _days.Add(day);
        }
        public void RemoveDay(LecturerDayVO day)
        {
            _days.Remove(day);
        }
        public List<LecturerDayVO> GetDays()
        {
            return _days.ToList();
        }

        public void SetDays(List<LecturerDayVO> days)
        {
            _days = days;
        }
    }
}
