﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class DatedMonthVO:IVObject
    {
        private int _id;
        private DataStatus _status;
        private Month _month;
        private int _hours;
        private BindedSubjectVO _bindedSubject;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public Month Month
        {
            get { return _month; }
            set
            {
                FunctionHelper.Set(value, _month, ref _status);
                _month = value;
            }
        }

        public int Hours
        {
            get { return _hours; }
            set
            {
                FunctionHelper.Set(value, _hours, ref _status);
                _hours = value;
            }
        }

        public BindedSubjectVO BindedSubject
        {
            get { return _bindedSubject; }
            set
            {
                FunctionHelper.Set(value.Id, _bindedSubject.Id, ref _status);
                _bindedSubject = value;
            }
        }
    }
}
