﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.BUS;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public class GroupVO : IVObject
    {
        private int _budgetCount;
        private int _contractCount;
        private int _admissionYear;
        private int _id;
        private DepartmentVO _department;
        private string _number;


        private DataStatus _status;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DepartmentVO Department
        {
            get { return _department; }
            set
            {
                FunctionHelper.Set(_department, value, ref _status);
                _department = value;
            }
        }

        public string Number
        {
            get { return _number; }
            set
            {
                FunctionHelper.Set(_number, value, ref _status);
                _number = value;
            }
        }

        public int AdmissionYear
        {
            get { return _admissionYear; }
            set
            {
                FunctionHelper.Set(_department, value, ref _status);
                _admissionYear = value;
            }
        }

        public int ContractCount
        {
            get { return _contractCount; }
            set
            {
                FunctionHelper.Set(_contractCount, value, ref _status);
                _contractCount = value;
            }
        }

        public int BudgetCount
        {
            get { return _budgetCount; }
            set
            {
                FunctionHelper.Set(_budgetCount, value, ref _status);
                _budgetCount = value;
            }
        }

        public string FullName
        {
            get { return _department.Name + "-" + _number; }
        }

        public DataStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int StudentsCount
        {
            get { return BudgetCount + ContractCount; }
        }

        public GroupVO()
        {
            Status = DataStatus.New;
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}
