﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using StudyLoad.DataHolders;
using System.Xml.Serialization;
namespace StudyLoad.VO
{
    public class LecturerVO:IVObject
    {
        private int _id;
        private DataStatus _status;
        private string _fio;
        private int _rate;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DataStatus Status
        {
            get
            {
                if(_status != DataStatus.New && _status != DataStatus.Removed)
                    if (BindedSubjects.IsDataChanged())//if any of asigned subjects was changed
                        return DataStatus.Updated;
                return _status;
            }
            set
            {
                _status = value;
                if(_status == DataStatus.Removed)
                    foreach (var subjectVO in BindedSubjects.Data)
                    {
                        BindedSubjects.Remove(subjectVO);
                    }
            }

        }

        public string FIO
        {
            get { return _fio; }
            set
            {
                FunctionHelper.Set(_fio, value, ref _status);
                _fio = value;
            }
        }


        public int Rate
        {
            get { return _rate; }
            set
            {
                FunctionHelper.Set(_rate, value, ref _status);
                 _rate = value;
            }
        }

        public int LoadSum
        {
            get { return BindedSubjects.Data.Sum(p => p.Subject.Overall); }
        }

        
        public DataManager<BindedSubjectVO> BindedSubjects;

        public LecturerVO()
        {
            _status = DataStatus.New;
            BindedSubjects = new DataManager<BindedSubjectVO>();
            
        }

        public LecturerVO(int id, string fio, int rate, DataStatus status)
            :this()
        {
            _rate = rate;
            _id = id;
            _status = status;
            _fio = fio;
        }
    }
}
