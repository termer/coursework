﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad.VO
{
    public interface IVObject
    {
        int Id { get; set; }
        DataStatus Status{ get; set; }
    }
}
