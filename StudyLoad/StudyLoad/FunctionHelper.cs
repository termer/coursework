﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using StudyLoad.DataHolders;

namespace StudyLoad
{
    public static class FunctionHelper
    {
        public static void  Set(object value, object value2, ref DataStatus status)
        {
            Debug.Assert(status != DataStatus.Removed, "Cannot edit removed data");
            if (value == value2) return;
            DataWasUpdated(ref status);
        }

        public static void DataWasUpdated(ref DataStatus status)
        {
            if (status == DataStatus.New || status == DataStatus.Removed)
                return;

            status = DataStatus.Updated;
        }
    }
}
