﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudyLoad.Controls
{
    partial class EditableList:ListView
    {
        public event EventHandler ElementChanged;

        private readonly TextBox editBox = new TextBox();
        private int X;
        private int Y;
        private ListViewItem li;
        private int subItemSelected;
        private string subItemText;

        public EditableList()
        {
            Dock = DockStyle.Fill;
         //   Font = new Font("Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            FullRowSelect = true;
            Name = "listView1";
            Size = new Size(0, 0);
            TabIndex = 0;
            View = View.Details;
            MouseDown += SMKMouseDown;
            DoubleClick += SMKDoubleClick;
            GridLines = true;

            editBox.Size = new Size(0, 0);
            editBox.Location = new Point(0, 0);
            Controls.AddRange(new Control[] {editBox});
            editBox.KeyPress += EditOver;
            editBox.LostFocus += FocusOver;
            editBox.Font = Font;//new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((0)));
            editBox.BackColor = Color.LightYellow;
            editBox.BorderStyle = BorderStyle.Fixed3D;
            editBox.Hide();
            editBox.Text = "";
        }



        private void EditOver(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                UpdateElement();
                editBox.Hide();
            }

            if (e.KeyChar == 27)
                editBox.Hide();
        }

        private void UpdateElement()
        {
            if (li.SubItems[subItemSelected].Text == editBox.Text) return;

            if (ElementChanged != null)
                ElementChanged(this, EventArgs.Empty);

            li.SubItems[subItemSelected].Text = editBox.Text;
        }


        private void FocusOver(object sender, EventArgs e)
        {
            UpdateElement();
            editBox.Hide();
        }

        public void SMKDoubleClick(object sender, EventArgs e)
        {
            int nStart = X;
            int spos = 0;
            int epos = Columns[0].Width;
            for (int i = 0; i < Columns.Count; i++)
            {
                if (nStart > spos && nStart < epos)
                {
                    subItemSelected = i;
                    break;
                }

                spos = epos;
                epos += Columns[i].Width;
            }

            subItemText = li.SubItems[subItemSelected].Text;

            var r = new Rectangle(spos, li.Bounds.Y, epos, li.Bounds.Bottom);
            editBox.Size = new Size(epos - spos, li.Bounds.Bottom - li.Bounds.Top);
            editBox.Location = new Point(spos, li.Bounds.Y);
            editBox.Show();
            editBox.Text = subItemText;
            editBox.SelectAll();
            editBox.Focus();
        }

        public void SMKMouseDown(object sender, MouseEventArgs e)
        {
            li = GetItemAt(e.X, e.Y);
            X = e.X;
            Y = e.Y;
        }
    }
}
