﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StudyLoad.Controls
{
    internal class ClosableTab : TabControl
    {
        public Image Image { get; set; }
        public Point ImageLocation { get; set; }

        public ClosableTab()
            : base()
        {
            ImageLocation = new Point(19, 9);
            DrawMode = TabDrawMode.OwnerDrawFixed;
            Padding = new Point(12, 3);
        }



        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            var r = GetTabRect(e.Index);

            e.Graphics.DrawString(TabPages[e.Index].Text,
                                  Font,
                                  new SolidBrush(Color.Black),
                                  new PointF(r.X, r.Y));

            e.Graphics.DrawImage(Image, GetImageLocation(e.Index));
        }


        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (Image == null) return;

            for (int i = 0; i < TabCount; i++)
            {
                var location = GetImageLocation(i);
                var size = Image.Size;
                var r = new Rectangle(location, size);
                r.Inflate(1,1);

                if (r.Contains(e.Location))
                {
                    CloseTab(i);
                    break;
                }
            }
        }

        private void CloseTab(int i)
        {
            TabPages.Remove(TabPages[i]);
        }

        private Point GetImageLocation(int i)
        {
            return new Point(GetTabRect(i).X + (GetTabRect(i).Width - ImageLocation.X), ImageLocation.Y);
        }
    }
}
